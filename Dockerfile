﻿FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443
ENV HEALTHCHECK_PORT=80

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /src
COPY ["SemanticKernel.csproj", "./"]
RUN dotnet restore "SemanticKernel.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "SemanticKernel.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "SemanticKernel.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app

ARG COMMIT_SHA
ARG PROJECT_VERSION=0
ARG BUILD_ID
ARG PIPELINE
ARG GIT_TAG
ARG TIMESTAMP
ARG BUILD_DATE

ENV ASPNETCORE_URLS=http://+:80
ENV COMMIT_SHA="${COMMIT_SHA}" \
    PROJECT_VERSION="${PROJECT_VERSION}" \
    BUILD_ID="${BUILD_ID}" \
    PIPELINE="${PIPELINE}" \
    GIT_TAG="${GIT_TAG}" \
    TIMESTAMP="${TIMESTAMP}" \
    BUILD_DATE="${BUILD_DATE}"

COPY --from=publish /app/publish .
# COPY src/Modules/SemanticKernel/Domain/Skills/SemanticSkills /src/Modules/SemanticKernel/Domain/Skills/SemanticSkills
ENTRYPOINT ["dotnet", "SemanticKernel.dll"]