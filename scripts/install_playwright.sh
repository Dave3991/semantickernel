#!/usr/bin/env bash
set -x
set -e
set -u
set -o pipefail

pwsh bin/Debug/net7.0/playwright.ps1 install
