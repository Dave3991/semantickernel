#!/usr/bin/env bash
set -x
set -e
set -u
set -o pipefail

# This script is used to start the LLMA API server.
sudo docker run --rm \
  -p 8000:8000 \
  -v /Users/david/RiderProjects/SemanticKernel/src/Modules/LlmaSharp/Domain/Models:/models \
  -e MODEL=/models/wizardlm-13b-v1.1-superhot-8k.ggmlv3.q4_0.bin \
  -e MAX_TOKEN_LENGTH=512 \
  --name llama-api-server \
   ghcr.io/abetlen/llama-cpp-python:latest
