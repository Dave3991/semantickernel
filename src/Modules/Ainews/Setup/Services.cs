using SemanticKernel.Modules.Ainews.Domain.Case.ScrapeRedditCase;
using SemanticKernel.Modules.Ainews.Domain.Case.ScrapeTwitterCase;
using SemanticKernel.Modules.Ainews.Domain.Case.TransformTextToVectors;
using SemanticKernel.Modules.Ainews.Domain.Case.TrendsCase;
using SemanticKernel.Modules.Ainews.Domain.DTOs;
using SemanticKernel.Modules.Ainews.Domain.Plugins;
using SemanticKernel.Modules.Ainews.Domain.Service;
using SemanticKernel.Modules.Ainews.Infrastructure.Ollama;
using SemanticKernel.Modules.Ainews.Infrastructure.Parsing;
using SemanticKernel.Modules.Ainews.Infrastructure.Parsing.RssReader;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.Persistance.PostgreSQL.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Services;
using SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.Operation;
using SemanticKernel.Modules.Ainews.UI.SignalR.Hubs;

namespace SemanticKernel.Modules.Ainews.Setup;

public static class Services
{
    public static string GetOpenAiApiKey()
    {
        return Environment.GetEnvironmentVariable("OPENAI_API_KEY") ?? throw new Exception("OPENAI_API_KEY is not set");
    }
    
    public static void ConfigureServices(IServiceCollection services)
    {
	    services.AddSingleton<ElasticConnector>();
        services.AddTransient<CreateKernel>();
        services.AddTransient<ScrapeRedditCase>();
        services.AddTransient<FindTopicService>();
        services.AddTransient<FindTopicsInTextPlugin>();
        services.AddTransient<SaveArticleTopicEntityOperation>();
        services.AddTransient<GetArticlesOperation>();
        services.AddTransient<GetArticleSourcesOperation>();
        services.AddTransient<GetArticlesBySourceOperation>();
        services.AddTransient<SaveArticleOperation>();
        services.AddTransient<ScrapeTwitterCase>();
        services.AddTransient<DeleteArticleOperation>();
        services.AddTransient<DeleteOldArticlesOperation>();
        services.AddTransient<SavingArticleService>();
        services.AddTransient<GenerateOperation>();
        services.AddTransient<GetArticleTopicsOperation>();
        services.AddTransient<GenerateNewsTrendsOperation>();
        services.AddTransient<TrendsCase>();
        services.AddTransient<ContentService>();
        services.AddTransient<ChatHub>();
        services.AddTransient<RssReader>();
        services.AddSingleton<ChatHistoryService>();
        
        // parsing services
        services.AddHostedService<FetchRssFeedService>();
        services.AddHostedService<ParsingRedditService>();
        services.AddHostedService<ParsingService>();
        services.AddHostedService<DeleteOldArticles>();

    }

  
}
