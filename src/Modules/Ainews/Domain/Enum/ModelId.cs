using DocumentFormat.OpenXml.Drawing;

namespace SemanticKernel.Modules.Ainews.Domain.Enum;

public class ModelId
{
    public const string EmbedingModel3Small = "text-embedding-3-small";
    public const string Gpt4Turbo = "gpt-4-turbo";
    public const string GPT4o = "gpt-4o";
}
