using System.Globalization;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using FluentResults;
using HtmlAgilityPack;
using OpenQA.Selenium.DevTools;
using SemanticKernel.Modules.Ainews.Domain.DTOs;
using SemanticKernel.Modules.Ainews.Domain.Entities;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;
using SemanticKernel.Modules.Ainews.Domain.Entities.TwitterApi;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.Persistance.PostgreSQL.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Services;

namespace SemanticKernel.Modules.Ainews.Domain.Case.ScrapeTwitterCase;

public class ScrapeTwitterCase
{
	public const string TwitterApiUrl = "http://tw.svarta.bauerdavid.com";
	public const string GetUserTweetsEndpoint = "/users/{screen_name}/last-tweet";

	private readonly ILogger<ScrapeTwitterCase> _logger;
	private readonly HttpClient _httpClient;
	private readonly SavingArticleService _saveArticleOperation;
	private readonly ContentService _fetchContent;

	public ScrapeTwitterCase(SavingArticleService saveArticleOperation,  ILogger<ScrapeTwitterCase> logger, ContentService fetchContent)
	{
		_saveArticleOperation = saveArticleOperation;
		_logger = logger;
		_fetchContent = fetchContent;
		_httpClient = new HttpClient();
	}

	public async Task<Result<TweetJsonResponse>> ExecuteAsync(ScrapeTwitterCaseDto request)
	{
		var url = TwitterApiUrl + GetUserTweetsEndpoint.Replace("{screen_name}", request.ScreenName);
		var response = await _httpClient.GetAsync(url);
		//ensure response is successful
		if (!response.IsSuccessStatusCode)
		{
			_logger.LogError("Failed to get tweet from Twitter API for screen name {ScreenName}, for Response {Response}", request.ScreenName, response);
			return FluentResults.Result.Fail("Failed to get tweet from Twitter API").WithError(response.ReasonPhrase).WithError(url);
		}
		var content = await response.Content.ReadAsStringAsync();
		TweetJsonResponse? tweet = null;
		try
		{
			tweet = JsonConvert.DeserializeObject<TweetJsonResponse>(content);
		}
		catch (Exception e)
		{
			var message = $"Failed to deserialize tweet from Twitter API for screen name {request.ScreenName}, for {content}, for Response {response}";
			_logger.LogError(e, message);
			return FluentResults.Result.Fail("Failed to deserialize tweet from Twitter API").WithError(e.Message).WithError(message).WithError(url);
		}
		
		if (tweet == null)
		{
			return  FluentResults.Result.Fail("Failed to get tweet from Twitter API");
		}

		

		string format = "ddd MMM dd HH:mm:ss zzzz yyyy";

		var publishDate = string.IsNullOrEmpty(tweet.CreatedAt)
			? DateTime.UtcNow
			: DateTime.ParseExact(tweet.CreatedAt, format, CultureInfo.InvariantCulture).ToUniversalTime();
		
		var tweetText = string.IsNullOrEmpty(tweet.FullText) ? tweet.Text : tweet.FullText;
		//if is url in content, parse it
		var urls = await ExtractUrls(tweetText);
		if (urls.Count > 0)
		{
			foreach (var url1 in urls)
			{
				var contentRes = await _fetchContent.FetchContentAsync(url1);
				if (contentRes.IsFailed)
				{
					_logger.LogError("{Errors}",JsonConvert.SerializeObject(contentRes.Errors));
				}
				else
				{
					_logger.LogDebug("Content fetched from {Url} is {Content}, original content: {OriginalContent}", url1, contentRes.Value, tweet.FullText);
					// add fetched content to tweet content
					tweet.FullText += contentRes.Value;
				}
			}
		}
		
		// save tweet to database as Article
		var article = new ArticleEntity
		{
			Content = tweet.FullText,
			ArticleUrl = tweet.Url,
			Sources = ["Twitter", tweet.ScreenName, tweet.UserName],
			Title = String.IsNullOrEmpty(tweet.Text) ? "" : tweet.Text,
			ImageUrl = tweet.Media?.FirstOrDefault()?.MediaUrlHttps ?? "",
			PublishDate = publishDate
		};
		
		var elasticArticle = new ElasticArticleEntity
		{
			Content = tweet.FullText,
			ArticleUrl = tweet.Url,
			Topics = request.Topics,
			Sources = ["Twitter", tweet.ScreenName, tweet.UserName],
			Title = String.IsNullOrEmpty(tweet.Text) ? "" : tweet.Text,
			ImageUrl = tweet.Media?.FirstOrDefault()?.MediaUrlHttps ?? "",
			PublishDate = publishDate,
			CustomData = new Dictionary<string, object>
			{
				{"tweet_id", tweet.Id},
				{"Retweets", tweet.RetweetCount},
				{"Likes", tweet.FavoriteCount}
			}
		};

		var res = await _saveArticleOperation.SaveArticleAsync(article, elasticArticle);
		if (res.IsFailed)
		{
			return  FluentResults.Result.Fail("Failed to save article to database");
		}
		
		return FluentResults.Result.Ok(tweet);
	}
	
	public async Task<List<string>> ExtractUrls(string text)
	{
		
		if (string.IsNullOrEmpty(text))
		{
			return new List<string>();
		}
		List<string> urls = new List<string>();

		// Define a regular expression to find URLs
		string pattern = @"(http|https)://[^\s/$.?#].[^\s]*";
		Regex regex = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);

		// Find matches in the input text
		var matches = regex.Matches(text);
		
		if (matches.Count == 0)
		{
			return urls;
		}
        
		foreach (Match match in matches)
		{
			string potentialUrl = match.Value;

			// Validate the URL
			if (Uri.IsWellFormedUriString(potentialUrl, UriKind.Absolute))
			{
				urls.Add(potentialUrl);
			}
		}

		return urls;
	}
}
