using Elastic.Clients.Elasticsearch;
using FluentResults;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.Persistance.PostgreSQL.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.DTOs;
using SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.Operation;
using Result = FluentResults.Result;

namespace SemanticKernel.Modules.Ainews.Domain.Case.TrendsCase;

public class TrendsCase
{
	
	private readonly GetArticleTopicsOperation _getArticleTopicsOperation;
	private readonly GetArticlesOperation _getArticlesOperation;
	private readonly GenerateNewsTrendsOperation _llmModelOperation;

	public TrendsCase(GetArticleTopicsOperation getArticleTopicsOperation, GetArticlesOperation getArticlesOperation, GenerateNewsTrendsOperation llmModelOperation)
	{
		_getArticleTopicsOperation = getArticleTopicsOperation;
		_getArticlesOperation = getArticlesOperation;
		_llmModelOperation = llmModelOperation;
	}

	// get all news from last 24 hours for given top topic, then send these news to llm model
	// to get the trends for the each topic and save them to the database
	public async Task<Result<List<NewsTrendsDto>>> GetTrendsAsync()
	{
		// get all top topics
		var topTopics = new string[]{ "Technology" };//await _getArticleTopicsOperation.GetTopTopicsAsync();
		// if (topTopics.IsFailed)
		// {
		// 	return Result.Fail("Failed to get top topics").WithErrors(topTopics.Errors);
		// }
		
		var trends = new List<NewsTrendsDto>();
		
		foreach (var topTopic in topTopics)
		{
			// get all news from last 24 hours for given top topic
			
			var news = await _getArticlesOperation.GetArticlessAsync(
				null, //new string[] {"topics"}!,
				topTopic,
				1,
				1000,
				DateTime.Now.AddDays(-1),
				DateTime.Now,
				new List<SortOptions>
				{
					SortOptions.Field(new Field("publishDate"), new FieldSort() {Order = SortOrder.Desc}),
				}
				);
			if (news.IsFailed)
			{
				return Result.Fail("Failed to get news").WithErrors(news.Errors);
			}
			
			// send these news to llm model to get the trends for the each topic
			var topicTrend = await _llmModelOperation.GenerateNewsTrendsAsync(news.Value, topTopic);
			if (topicTrend.IsFailed)
			{
				return Result.Fail("Failed to get trends").WithErrors(topicTrend.Errors);
			}
			
			trends.Add(topicTrend.Value);
			
			// save trends to the database
			// var saveTrendsResult = await _saveTrendsOperation.SaveTrendsAsync(trends.Value);
			// if (saveTrendsResult.IsFailed)
			// {
			// 	return Result.Fail("Failed to save trends");
			// }
		}
		
		return Result.Ok(trends);
	}
	
}
