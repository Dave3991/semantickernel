using Microsoft.KernelMemory;

namespace SemanticKernel.Modules.Ainews.Domain.Case.TransformTextToVectors.DTO;

public record TransformTextToVectorsDto
{
    public string TextToSave { get; init; }
    public TagCollection Tags { get; init; }
}