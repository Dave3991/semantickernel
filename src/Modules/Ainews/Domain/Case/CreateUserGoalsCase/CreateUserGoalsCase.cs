using Microsoft.SemanticKernel;
using SemanticKernel.Modules.Ainews.Domain.Service;

namespace SemanticKernel.Modules.Ainews.Domain.Case.CreateUserGoalsCase;

/// <summary>
/// Goal of this case is to create user goals based on the user's preferences.
/// </summary>
public class CreateUserGoalsCase
{
    private readonly ILogger<CreateUserGoalsCase> _logger;
    
    private readonly CreateKernel _createKernel;

    public CreateUserGoalsCase(ILogger<CreateUserGoalsCase> logger, CreateKernel createKernel)
    {
        _logger = logger;
        _createKernel = createKernel;
    }

    public async Task Execute(string userContext)
    {
        var kernel = _createKernel.CreateKernelBuilder().Build();
        //kernel.AskAsync(userContext);
    }
}