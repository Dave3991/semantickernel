using System.Text.RegularExpressions;
using FluentResults;
using HtmlAgilityPack;
using Microsoft.KernelMemory;
using Newtonsoft.Json;
using SemanticKernel.Modules.Ainews.Domain.Entities;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;
using SemanticKernel.Modules.Ainews.Domain.Plugins;
using SemanticKernel.Modules.Ainews.Domain.Service;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.Persistance.PostgreSQL.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Services;
using StringReader = System.IO.StringReader;

namespace SemanticKernel.Modules.Ainews.Domain.Case.ScrapeRedditCase;

/// <summary>
/// Class which is responsible for scraping Reddit News data from given url https://www.reddit.com/r/worldnews.json and push it to the memory.
/// </summary>
public class ScrapeRedditCase
{
	private readonly ILogger<ScrapeRedditCase> _logger;

	private readonly FindTopicsInTextPlugin _findTopicsInTextPlugin;

	private readonly SavingArticleService _savingArticleService;

	public ScrapeRedditCase(ILogger<ScrapeRedditCase> logger,
		FindTopicsInTextPlugin findTopicsInTextPlugin,
		SavingArticleService savingArticleService)
	{
		_logger = logger;
		_findTopicsInTextPlugin = findTopicsInTextPlugin;
		_savingArticleService = savingArticleService;
	}

	public async Task<string> Execute()
	{
		// TODO: AI will be used to find topics in the text from given group of topics
		var topics = new Dictionary<string, List<string>>
		{
			//Technology
			{ "Technology", new List<string> { "technology" } },
			{ "technews", new List<string> { "technology", "technews" } },
			{ "Futurology", new List<string> { "technology", "futurology" } },
			{ "gadgets", new List<string> { "technology", "gadgets" } },
			{ "programming", new List<string> { "technology", "programming" } },
			{ "hardware", new List<string> { "technology", "hardware" } },
			{ "software", new List<string> { "technology", "software" } },

			// Motivation
			{ "Motivation", new List<string> { "Motivation" } },
			// { "GetMotivated", new List<string> { "Motivation", "GetMotivated" } },
			// { "Inspiration", new List<string> { "Motivation", "Inspiration" } },
			// { "MotivationalPics", new List<string> { "Motivation", "Motivational" } },
			// { "InspirationalQuotes", new List<string> { "Motivation", "InspirationalQuotes" } },
			// { "GetDisciplined", new List<string> { "Motivation", "GetDisciplined" } },
			// { "Mindset", new List<string> { "Motivation", "Mindset" } },

			// Memes
			// { "meme", new List<string> { "Memes" } },
			// { "memes", new List<string> { "Memes" } },
			// { "dankmemes", new List<string> { "Memes", "DankMemes" } },
			// { "wholesomememes", new List<string> { "Memes", "WholesomeMemes" } },


			// Science
			{ "EverythingScience", new List<string> { "science", "everythingscience" } },
			{ "science", new List<string> { "science" } },

			// // Entertainment
			// { "movies", new List<string> { "entertainment", "movies" } },
			// { "television", new List<string> { "entertainment", "television" } },
			// { "music", new List<string> { "entertainment", "music" } },
			// { "celebrities", new List<string> { "entertainment", "celebrities" } },
			// { "entertainment", new List<string> { "entertainment" } },
			//
			// // Sports
			// { "sports", new List<string> { "sports" } },
			// { "nfl", new List<string> { "nfl" } },
			// { "nba", new List<string> { "nba" } },
			// { "soccer", new List<string> { "soccer" } },
			// { "baseball", new List<string> { "baseball" } },
			// { "hockey", new List<string> { "hockey" } },
			// { "mma", new List<string> { "mma" } },
			// { "golf", new List<string> { "golf" } },
			// { "tennis", new List<string> { "tennis" } },
			// { "olympics", new List<string> { "olympics" } },
			// { "sportsnews", new List<string> { "sportsnews" } },
			//
			// // Lifestyle
			// { "lifestyle", new List<string> { "lifestyle" } },
			// { "food", new List<string> { "lifestyle", "food" } },
			// { "travel", new List<string> { "lifestyle", "travel" } },
			// { "fitness", new List<string> { "lifestyle", "fitness" } },
			// { "fashion", new List<string> { "lifestyle", "fashion" } },
			//
			//
			// // Gaming
			// { "gamingnews", new List<string> { "gaming" } },
			// { "mobilegaming", new List<string> { "Mobile Gaming" } },
			// { "consolegaming", new List<string> { "Console Gaming" } },
			// { "pcgaming", new List<string> { "PC Gaming" } },
			// { "gaming", new List<string> { "gaming" } },
			//
			// //Education
			// { "education", new List<string> { "education" } },

			// News
			{ "worldnews", new List<string> { "news", "worldnews" } },
			{ "UpliftingNews", new List<string> { "news", "upliftingnews" } },
			{ "NewsPorn", new List<string> { "news", "newsporn" } },
			{ "InternationalNews", new List<string> { "news", "internationalnews" } },
			{ "news", new List<string> { "news" } },
			// {"AnythingGoesNews", new List<string> {"anythinggoesnews"}},
			{ "WorldNews_Serious", new List<string> { "news", "World News" } },
			{ "goodnews", new List<string> { "news", "goodnews" } },
			{ "NewsOfTheStupid", new List<string> { "newsofthestupid" } },
			{ "NewsOfTheWeird", new List<string> { "newsoftheweird" } },
			{ "stocknews", new List<string> { "stocks", "news", "stocknews" } },
			// život je nejčastější smrtelná choroba, přenášená pohlavni stykem

			// Business
			{ "business", new List<string> { "business" } },
			{ "economics", new List<string> { "economics" } },
			{ "finance", new List<string> { "finance" } },
			{ "investing", new List<string> { "investing" } },
			{ "stocks", new List<string> { "stocks" } },
			{ "wallstreet", new List<string> { "stocks", "wallstreet" } },
			{ "wallstreetbets", new List<string> { "stocks", "wallstreetbets" } },

			//Art
			// { "art", new List<string>() { "art" } },
			//
			// //health
			// { "Health", new List<string> { "health" } },
			// { "WeightLossNews", new List<string> { "health", "weightloss" } },
			// { "privacy", new List<string> { "privacy" } },

			// crypto
			{ "Crypto_Currency_News", new List<string> { "crypto", "cryptocurrency" } },
			{ "CryptoNews", new List<string> { "crypto", "cryptocurrency" } },
			{ "altcoin_news", new List<string> { "crypto", "cryptocurrency", "altcoin" } },
		};

		// Iterate over the dictionary and call LoadDataFromReddit for each topic and its subtopics
		foreach (var topic in topics)
		{
			var redditUrl = $"https://www.reddit.com/r/{topic.Key}.json";
			var result = await LoadDataFromReddit(redditUrl, topic.Value);
			if (result.IsFailed)
			{
				_logger.LogError("Errored because {ResultErrors} at {RedditUrl}",  result.Errors, redditUrl);
			}
		}

		return "ok";
	}

	public async Task<FluentResults.Result> LoadDataFromReddit(string redditUrl, List<string> topics)
	{
		using var client = new HttpClient();
		client.Timeout = TimeSpan.FromMinutes(3);
		client.DefaultRequestHeaders.UserAgent.ParseAdd(
			"YuvggqE1U1TlBj5qlo_y27TuNgLpKw v1.0"); // replace "YourAppName v1.0" with your actual app name and version
		var response = await client.GetAsync(redditUrl);
		var content = await response.Content.ReadAsStringAsync();

		RedditResponse json = null;
		try
		{
			var jsonReader = new JsonTextReader(new StringReader(content));
			var serializer = new JsonSerializer();
			json = serializer.Deserialize<RedditResponse>(jsonReader) ?? throw new JsonReaderException("Failed to parse JSON from Reddit API response for " + redditUrl);
		}
		catch (JsonReaderException ex)
		{
			_logger.LogError(ex, "Failed to parse JSON from Reddit API response for {RedditUrl}, for Content: {Content}", redditUrl, content);
			return FluentResults.Result.Fail(new Error("Failed to parse JSON from Reddit API response.").CausedBy(ex)).WithError(ex.Message);
		}

		var news = json?.Data?.Children.Select(x => x.Data).ToList();

		if (news == null)
		{
			_logger.LogWarning("No news found for {RedditUrl}", redditUrl);
			return FluentResults.Result.Fail("No news found");
		}

		foreach (var item in news)
		{
			//replace subreddit name from title to make it more clear, ignore case
			var clearTitle = Regex.Replace(item.Title, "/" + item.Subreddit_name_prefixed, "", RegexOptions.IgnoreCase)
				.Trim();
			_logger.LogInformation(clearTitle);

			var redditCustomData = new Dictionary<string, object>();
			redditCustomData.Add("upvote_ratio", item.Upvote_ratio);
			redditCustomData.Add("ups", item.Ups);
			redditCustomData.Add("downs", item.Downs);
			redditCustomData.Add("score", item.Score);
			redditCustomData.Add("num_comments", item.num_comments);

			// Saving article to the SQL database only because we want to be sure that we don't have duplicates
			var result = await _savingArticleService.SaveArticleAsync(new ArticleEntity()
			{
				Content = clearTitle,
				ArticleUrl = item.Url,
				Title = clearTitle,
				ImageUrl = item.Preview?.Images?.FirstOrDefault()?.Source?.Url ?? "",
				Sources = new List<string> { "reddit", item.Subreddit },
				PublishDate = DateTime.UtcNow
			},
				new ElasticArticleEntity() {
					Content = clearTitle,
					ArticleUrl = item.Url,
					ImageUrl = item.UrlOverriddenByDest ?? "",
					Topics = topics,
					Sources = new List<string> { "reddit", item.Subreddit },
					Title = clearTitle,
					PublishDate = DateTime.UtcNow,
					CustomData = redditCustomData
				}
				);
			if (result.IsFailed)
			{
				var errorsJson = JsonConvert.SerializeObject(result.Errors);
				_logger.LogError("Errors: {ErrorsJson} at URL: {ItemUrl}", errorsJson, item.Url);
				continue;
			}

			var description = await LoadDescription(item.Url, redditUrl, topics);
			if (description.IsFailed)
			{
				_logger.LogWarning("Description is null for {Url}", item.Url);
			}

			// await _memory.ImportTextAsync(clearTitle + " " + description, tags: new() {
			//     {"clearTitle", clearTitle},
			//     { "source", "reddit" },
			//     { "upvote_ratio", item.Upvote_ratio.ToString()},
			//     { "ups", item.Ups.ToString()},
			//     { "downs", item.Downs.ToString()},
			//     { "score", item.Score.ToString()},
			//     { "subreddit", item.Subreddit },
			//     { "url", item.Url },
			//     { "date", DateTime.UtcNow.ToString("o") }, // "2024-04-08T13:54:26.0291210Z"
			// });
		}

		return FluentResults.Result.Ok();
	}

	public async Task<FluentResults.Result<string>> LoadDescription(string url, string source, List<string> topics)
	{
		var handler = new HttpClientHandler()
		{
			ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true,
			AllowAutoRedirect = true,
			MaxAutomaticRedirections = 10
		};

		var client = new HttpClient(handler) { Timeout = TimeSpan.FromMinutes(3) };

		// Add headers to imitate a browser
		client.DefaultRequestHeaders.UserAgent.ParseAdd(
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3");
		client.DefaultRequestHeaders.Accept.ParseAdd(
			"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		client.DefaultRequestHeaders.AcceptLanguage.ParseAdd("en-US,en;q=0.5");
		client.DefaultRequestHeaders.Connection.ParseAdd("keep-alive");
		client.DefaultRequestHeaders.Host = new Uri(url).Host;

		var retries = 3;
		for (var i = 0; i < retries; i++)
		{
			try
			{
				var result = await AttemptLoadDescription(client, url, source, topics);
				if (result.IsSuccess)
				{
					return result.Value;
				}

				_logger.LogWarning("Attempt {Attempt} failed: {Error}", i + 1, result.Errors);
			}
			catch (Exception ex)
			{
				_logger.LogWarning("Attempt {Attempt} threw an exception: {Exception}", i + 1, ex);
			}
		}

		return FluentResults.Result.Fail<string>("Failed to load description after retries");
	}

	private async Task<FluentResults.Result<string>> AttemptLoadDescription(HttpClient client, string url,
		string source, List<string> topics)
	{
		if (IsImageUrl(url))
		{
			return await HandleImageSource(client, url, source, topics);
		}

		return await HandleWebPageSource(client, url, source, topics);
	}

	private bool IsImageUrl(string url)
	{
		return url.EndsWith(".jpg") || url.EndsWith(".png") || url.EndsWith(".gif") || url.EndsWith(".jpeg");
	}

	private async Task<FluentResults.Result<string>> HandleImageSource(HttpClient client, string url, string source,
		List<string> topics)
	{
		var article = new ElasticArticleEntity()
		{
			Content = "",
			Sources = new List<string> { source, url },
			PublishDate = DateTime.UtcNow,
			Title = "",
			ImageUrl = url,
			Topics = topics,
			ArticleUrl = url
		};

		var postgreArticle = new ArticleEntity()
		{
			Content = "",
			ArticleUrl = url,
			Title = "",
			ImageUrl = url,
			Sources = new List<string> { source },
			PublishDate = DateTime.UtcNow
		};
		var saveResult = await _savingArticleService.SaveArticleAsync(postgreArticle, article);
		if (saveResult.IsSuccess)
		{
			return FluentResults.Result.Ok("Url points to image");
		}

		return FluentResults.Result.Fail<string>("Failed to save article");
	}

	private async Task<FluentResults.Result<string>> HandleWebPageSource(HttpClient client, string url, string source,
		List<string> topics)
	{
		var response = await client.GetAsync(url);
		var content = await response.Content.ReadAsStringAsync();
		var doc = new HtmlDocument();
		doc.LoadHtml(content);
		var description = doc.DocumentNode.SelectSingleNode("//meta[@name='description']");

		if (description != null && description.Attributes.Contains("content"))
		{
			return await SaveWebPageArticle(doc, description, url, source, topics);
		}

		return FluentResults.Result.Fail<string>("Description not found");
	}

	private async Task<FluentResults.Result<string>> SaveWebPageArticle(HtmlDocument doc, HtmlNode description,
		string url, string source, List<string> topics)
	{
		var title = doc.DocumentNode.SelectSingleNode("//title")?.InnerText ?? "No title";
		var pageContent = description.Attributes["content"].Value;

		var article = new ElasticArticleEntity()
		{
			Content = pageContent,
			Sources = new List<string> { source, url },
			PublishDate = DateTime.UtcNow,
			Title = title,
			ImageUrl = doc.DocumentNode.SelectSingleNode("//meta[@property='og:image']")
				?.Attributes["content"]?.Value ?? "",
			Topics = topics,
			ArticleUrl = url,
		};
		
		var postgreArticle = new ArticleEntity()
		{
			Content = "",
			ArticleUrl = url,
			Title = "",
			ImageUrl = url,
			Sources = new List<string> { source },
			PublishDate = DateTime.UtcNow
		};
		
		

		var saveResult = await _savingArticleService.SaveArticleAsync(postgreArticle, article);
		if (saveResult.IsSuccess)
		{
			return FluentResults.Result.Ok(pageContent);
		}

		_logger.LogError("Failed to save article: {Errors}", saveResult.Errors);
		return FluentResults.Result.Fail<string>("Failed to save article");
	}
}
