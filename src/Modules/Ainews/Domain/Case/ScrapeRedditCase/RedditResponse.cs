using Newtonsoft.Json;

public class RedditResponse
{
    [JsonProperty("kind")]
    public string Kind { get; set; }

    [JsonProperty("data")]
    public Data Data { get; set; }
}

public class Data
{
    [JsonProperty("after")]
    public string After { get; set; }

    [JsonProperty("dist")]
    public int Dist { get; set; }

    [JsonProperty("modhash")]
    public string Modhash { get; set; }

    [JsonProperty("geo_filter")]
    public object Geo_filter { get; set; }

    [JsonProperty("children")]
    public List<Child> Children { get; set; }
}

public class Child
{
    [JsonProperty("kind")]
    public string Kind { get; set; }

    [JsonProperty("data")]
    public ChildData Data { get; set; }
}

public class Preview
{
	[JsonProperty("images")]
	public List<Image> Images { get; set; }

	[JsonProperty("enabled")]
	public bool Enabled { get; set; }
}

public class Image
{
	[JsonProperty("source")]
	public ImageSource Source { get; set; }

	[JsonProperty("resolutions")]
	public List<Resolution> Resolutions { get; set; }

	[JsonProperty("variants")]
	public object Variants { get; set; } // Adjust this based on actual variants structure

	[JsonProperty("id")]
	public string Id { get; set; }
}

public class Resolution : ImageSource
{
	// Inherits Url, Width, and Height from ImageSource
}

public class ImageSource
{
	[JsonProperty("url")]
	public string Url { get; set; }

	[JsonProperty("width")]
	public int Width { get; set; }

	[JsonProperty("height")]
	public int Height { get; set; }
}

public class ChildData
{
	[JsonProperty("num_comments")]
	public int num_comments;

	[JsonProperty("subreddit")]
    public string Subreddit { get; set; }

    [JsonProperty("title")]
    public string Title { get; set; }
    
    [JsonProperty("subreddit_name_prefixed")]
    public string Subreddit_name_prefixed { get; set; }

    [JsonProperty("saved")]
    public bool Saved { get; set; }

    [JsonProperty("gilded")]
    public int Gilded { get; set; }

    [JsonProperty("clicked")]
    public bool Clicked { get; set; }

    [JsonProperty("author_fullname")]
    public string Author_fullname { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("quarantine")]
    public bool Quarantine { get; set; }

    [JsonProperty("upvote_ratio")]
    public double Upvote_ratio { get; set; }

    [JsonProperty("ups")]
    public int Ups { get; set; }

    [JsonProperty("total_awards_received")]
    public int Total_awards_received { get; set; }

    [JsonProperty("thumbnail")]
    public string Thumbnail { get; set; }

    [JsonProperty("is_video")]
    public bool Is_video { get; set; }

    [JsonProperty("permalink")]
    public string Permalink { get; set; }

    [JsonProperty("url")]
    public string Url { get; set; }
    
    [JsonProperty("url_overridden_by_dest")]
    public string? UrlOverriddenByDest { get; set; }

    [JsonProperty("score")]
    public int Score { get; set; }
    
    [JsonProperty("downs")]
    public int Downs { get; set; }
    
    [JsonProperty("preview")]
    public Preview Preview { get; set; }
}
