using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SemanticKernel.Modules.Ainews.Domain.Entities.Postgres
{
    [Table("users")]
    public class UserEntity
    {
        [Key]
        [Column("user_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; init; }

        [Column("email")]
        public string Email { get; set; }

        // other user fields

        public ICollection<CollectionEntity> Collections { get; set; } = new List<CollectionEntity>();
    }
}
