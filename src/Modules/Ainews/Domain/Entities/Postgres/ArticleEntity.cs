using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using System.Text;

namespace SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;

[Table("articles")]
public class ArticleEntity
{
	[Key]
	[Column("article_id")]
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int Id { get; set; }

	[Column("title")]
	public required string Title { get; set; }

	private string _content;

	[Column("content")]
	public string Content
	{
		get => _content;
		set
		{
			_content = value;
			ContentHash = ComputeHash(_content);
		}
	}

	[Column("content_hash")]
	public string ContentHash { get; private set; } 

	[Column("image_url")]
	public required string ImageUrl { get; set; }

	// ToDO: Change to ArticleUrl
	[Column("ArticleUrl")]
	public required string ArticleUrl { get; set; }

	[Column("publish_date")]
	public required DateTime PublishDate { get; set; }

	public ICollection<ArticleTopicEntity> ArticleTopics { get; set; } = new List<ArticleTopicEntity>();

	//ignore column Sources in the database
	[NotMapped]
	public List<string> Sources { get; set; }

	[Column("elastic_article_id")]
	public Guid ElasticArticleId { get; set; }

	private string ComputeHash(string input)
	{
		using (var sha256 = SHA256.Create())
		{
			var bytes = Encoding.UTF8.GetBytes(input);
			var hash = sha256.ComputeHash(bytes);
			return Convert.ToBase64String(hash);
		}
	}
}
