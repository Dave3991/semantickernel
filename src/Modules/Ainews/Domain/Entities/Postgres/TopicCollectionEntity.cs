using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SemanticKernel.Modules.Ainews.Domain.Entities.Postgres
{
    [Table("topic_collections")]
    public class TopicCollectionEntity
    {
        [Key]
        [Column("collection_id", Order = 0)]
        public int CollectionId { get; set; }

        [Key]
        [Column("topic_id", Order = 1)]
        public int TopicId { get; set; }
        
        // refers to position of the topic in columns
        [Column("column_index")]
        public int ColumnIndex { get; set; }

        public CollectionEntity Collection { get; set; }

        public TopicEntity Topic { get; set; }
    }
}
