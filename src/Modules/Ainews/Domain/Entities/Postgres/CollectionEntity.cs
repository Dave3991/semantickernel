using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SemanticKernel.Modules.Ainews.Domain.Entities.Postgres
{
    [Table("collections")]
    public class CollectionEntity
    {
        [Key]
        [Column("collection_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; init; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("collection_name")]
        public string CollectionName { get; set; }

        // other collection fields

        public required UserEntity User { get; set; }

        public ICollection<TopicCollectionEntity> TopicCollections { get; set; } = new List<TopicCollectionEntity>();
    }
}
