using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DocumentFormat.OpenXml.Spreadsheet;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;

namespace SemanticKernel.Modules.Ainews.Domain.Entities;

[Table("article_topics")]
public class ArticleTopicEntity
{
    [Key]
    [Column("article_id")]
    public int ArticleId { get; set; }
    
    public ArticleEntity Article { get; set; }

    [Key]
    [Column("topic_id")]
    public int TopicID { get; set; }
    
    public TopicEntity Topic { get; set; }
}