using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SemanticKernel.Modules.Ainews.Domain.Enum;

namespace SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;

[Table("topics")]
public class TopicEntity
{
    
    public ICollection<ArticleTopicEntity> ArticleTopics { get; set; } = new List<ArticleTopicEntity>();
    
    public ICollection<TopicCollectionEntity> TopicCollections { get; set; } = new List<TopicCollectionEntity>();
    
    [Key]
    [Column("topic_id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; init; }
    
    [Required]
    [StringLength(255)]
    [Column("name")]
    public required string Name { get; set; }
    
    [Column("is_enabled")]
    public bool IsEnabled { get; private set; } = true;
    
    public int? ParentId { get; set; }

    [Required]
    [Column("type")]
    public TopicNodeTypesEnum Type { get; set; }

    [ForeignKey("ParentId")]
    public virtual TopicEntity Parent { get; set; }

    public virtual ICollection<TopicEntity> Children { get; set; }

    public TopicEntity()
    {
	    Children = new List<TopicEntity>();
    }
}
