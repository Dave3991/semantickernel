using Newtonsoft.Json;

namespace SemanticKernel.Modules.Ainews.Domain.Entities.TwitterApi;

public class TweetJsonResponse
{
	[JsonProperty("created_at")]
	public string CreatedAt { get; set; }

	[JsonProperty("favorite_count")]
	public int FavoriteCount { get; set; }

	[JsonProperty("full_text")]
	public string FullText { get; set; }

	[JsonProperty("id")]
	public string Id { get; set; }

	[JsonProperty("lang")]
	public string Lang { get; set; }

	[JsonProperty("media")]
	public List<Media> Media { get; set; }

	[JsonProperty("reply_count")]
	public int ReplyCount { get; set; }

	[JsonProperty("retweet_count")]
	public List<int>? RetweetCount { get; set; }

	[JsonProperty("screen_name")]
	public string ScreenName { get; set; }

	[JsonProperty("text")]
	public string Text { get; set; }

	[JsonProperty("url")]
	public string Url { get; set; }

	[JsonProperty("user_id")]
	public string UserId { get; set; }

	[JsonProperty("user_name")]
	public string UserName { get; set; }
}

public class Media
{
	[JsonProperty("additional_media_info")]
	public AdditionalMediaInfo AdditionalMediaInfo { get; set; }

	[JsonProperty("display_url")]
	public string DisplayUrl { get; set; }

	[JsonProperty("expanded_url")]
	public string ExpandedUrl { get; set; }

	[JsonProperty("ext_media_availability")]
	public ExtMediaAvailability ExtMediaAvailability { get; set; }

	[JsonProperty("id_str")]
	public string IdStr { get; set; }

	[JsonProperty("indices")]
	public List<int> Indices { get; set; }

	[JsonProperty("media_key")]
	public string MediaKey { get; set; }

	[JsonProperty("media_url_https")]
	public string MediaUrlHttps { get; set; }

	[JsonProperty("original_info")]
	public OriginalInfo OriginalInfo { get; set; }

	[JsonProperty("sizes")]
	public Sizes Sizes { get; set; }

	[JsonProperty("source_status_id_str")]
	public string SourceStatusIdStr { get; set; }

	[JsonProperty("source_user_id_str")]
	public string SourceUserIdStr { get; set; }

	[JsonProperty("type")]
	public string Type { get; set; }

	[JsonProperty("url")]
	public string MediaUrl { get; set; }

	[JsonProperty("video_info")]
	public VideoInfo VideoInfo { get; set; }
}

public class AdditionalMediaInfo
{
	[JsonProperty("monetizable")]
	public bool Monetizable { get; set; }

	[JsonProperty("source_user")]
	public SourceUser SourceUser { get; set; }
}

public class SourceUser
{
	[JsonProperty("user_results")]
	public UserResults UserResults { get; set; }
}

public class UserResults
{
	[JsonProperty("result")]
	public Result Result { get; set; }
}

public class Result
{
	[JsonProperty("__typename")]
	public string Typename { get; set; }

	[JsonProperty("affiliates_highlighted_label")]
	public object AffiliatesHighlightedLabel { get; set; }

	[JsonProperty("has_graduated_access")]
	public bool HasGraduatedAccess { get; set; }

	[JsonProperty("id")]
	public string Id { get; set; }

	[JsonProperty("is_blue_verified")]
	public bool IsBlueVerified { get; set; }

	[JsonProperty("legacy")]
	public Legacy Legacy { get; set; }

	[JsonProperty("professional")]
	public Professional Professional { get; set; }

	[JsonProperty("profile_image_shape")]
	public string ProfileImageShape { get; set; }

	[JsonProperty("rest_id")]
	public string RestId { get; set; }
}

public class Legacy
{
	[JsonProperty("can_dm")]
	public bool CanDm { get; set; }

	[JsonProperty("can_media_tag")]
	public bool CanMediaTag { get; set; }

	[JsonProperty("created_at")]
	public string CreatedAt { get; set; }

	[JsonProperty("default_profile")]
	public bool DefaultProfile { get; set; }

	[JsonProperty("default_profile_image")]
	public bool DefaultProfileImage { get; set; }

	[JsonProperty("description")]
	public string Description { get; set; }

	[JsonProperty("entities")]
	public Entities Entities { get; set; }

	[JsonProperty("fast_followers_count")]
	public int FastFollowersCount { get; set; }

	[JsonProperty("favourites_count")]
	public int FavouritesCount { get; set; }

	[JsonProperty("followers_count")]
	public int FollowersCount { get; set; }

	[JsonProperty("friends_count")]
	public int FriendsCount { get; set; }

	[JsonProperty("has_custom_timelines")]
	public bool HasCustomTimelines { get; set; }

	[JsonProperty("is_translator")]
	public bool IsTranslator { get; set; }

	[JsonProperty("listed_count")]
	public int ListedCount { get; set; }

	[JsonProperty("location")]
	public string Location { get; set; }

	[JsonProperty("media_count")]
	public int MediaCount { get; set; }

	[JsonProperty("name")]
	public string Name { get; set; }

	[JsonProperty("normal_followers_count")]
	public int NormalFollowersCount { get; set; }

	[JsonProperty("pinned_tweet_ids_str")]
	public List<string> PinnedTweetIdsStr { get; set; }

	[JsonProperty("possibly_sensitive")]
	public bool PossiblySensitive { get; set; }

	[JsonProperty("profile_banner_url")]
	public string ProfileBannerUrl { get; set; }

	[JsonProperty("profile_image_url_https")]
	public string ProfileImageUrlHttps { get; set; }

	[JsonProperty("profile_interstitial_type")]
	public string ProfileInterstitialType { get; set; }

	[JsonProperty("screen_name")]
	public string ScreenName { get; set; }

	[JsonProperty("statuses_count")]
	public int StatusesCount { get; set; }

	[JsonProperty("translator_type")]
	public string TranslatorType { get; set; }

	[JsonProperty("url")]
	public string Url { get; set; }

	[JsonProperty("verified")]
	public bool Verified { get; set; }

	[JsonProperty("verified_type")]
	public string VerifiedType { get; set; }

	[JsonProperty("want_retweets")]
	public bool WantRetweets { get; set; }

	[JsonProperty("withheld_in_countries")]
	public List<string> WithheldInCountries { get; set; }
}

public class Entities
{
	[JsonProperty("description")]
	public Description Description { get; set; }

	[JsonProperty("url")]
	public Url Url { get; set; }
}

public class Description
{
	[JsonProperty("urls")]
	public List<object> Urls { get; set; }
}

public class Url
{
	[JsonProperty("urls")]
	public List<UrlInfo> Urls { get; set; }
}

public class UrlInfo
{
	[JsonProperty("display_url")]
	public string DisplayUrl { get; set; }

	[JsonProperty("expanded_url")]
	public string ExpandedUrl { get; set; }

	[JsonProperty("indices")]
	public List<int> Indices { get; set; }

	[JsonProperty("url")]
	public string Url { get; set; }
}

public class Professional
{
	[JsonProperty("category")]
	public List<Category> Category { get; set; }

	[JsonProperty("professional_type")]
	public string ProfessionalType { get; set; }

	[JsonProperty("rest_id")]
	public string RestId { get; set; }
}

public class Category
{
	[JsonProperty("icon_name")]
	public string IconName { get; set; }

	[JsonProperty("id")]
	public int Id { get; set; }

	[JsonProperty("name")]
	public string Name { get; set; }
}

public class ExtMediaAvailability
{
	[JsonProperty("status")]
	public string Status { get; set; }
}

public class OriginalInfo
{
	[JsonProperty("focus_rects")]
	public List<object> FocusRects { get; set; }

	[JsonProperty("height")]
	public int Height { get; set; }

	[JsonProperty("width")]
	public int Width { get; set; }
}

public class Sizes
{
	[JsonProperty("large")]
	public Size Large { get; set; }

	[JsonProperty("medium")]
	public Size Medium { get; set; }

	[JsonProperty("small")]
	public Size Small { get; set; }

	[JsonProperty("thumb")]
	public Size Thumb { get; set; }
}

public class Size
{
	[JsonProperty("h")]
	public int Height { get; set; }

	[JsonProperty("resize")]
	public string Resize { get; set; }

	[JsonProperty("w")]
	public int Width { get; set; }
}

public class VideoInfo
{
	[JsonProperty("aspect_ratio")]
	public List<int> AspectRatio { get; set; }

	[JsonProperty("duration_millis")]
	public int DurationMillis { get; set; }

	[JsonProperty("variants")]
	public List<Variant> Variants { get; set; }
}

public class Variant
{
	[JsonProperty("content_type")]
	public string ContentType { get; set; }

	[JsonProperty("url")]
	public string Url { get; set; }

	[JsonProperty("bitrate")]
	public int? Bitrate { get; set; }
}
