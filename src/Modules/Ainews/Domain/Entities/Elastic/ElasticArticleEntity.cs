using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;

public class ElasticArticleEntity
{
	[JsonProperty("id")]
	public Guid Id { get; set; } = Guid.NewGuid();

	[JsonProperty("title", Required = Required.Always)]
	public string Title { get; set; }

	[JsonProperty("content", Required = Required.Always)]
	public string Content { get; set; }

	[JsonProperty("imageUrl", NullValueHandling = NullValueHandling.Ignore)]
	public string ImageUrl { get; set; }

	[JsonProperty("articleUrl", Required = Required.Always)]
	public string ArticleUrl { get; set; }

	[JsonProperty("sources", Required = Required.Always)]
	public List<string> Sources { get; set; }

	[JsonProperty("publishDate")]
	public DateTime PublishDate { get; set; }

	[JsonProperty("topics", Required = Required.Always)]
	public List<string> Topics { get; set; }

	[JsonProperty("customData")]
	public Dictionary<string, object> CustomData { get; set; } = new Dictionary<string, object>();
	
}
