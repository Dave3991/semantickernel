namespace SemanticKernel.Modules.Ainews.Domain.DTOs;

public class ScrapeTwitterCaseDto
{
	public required string ScreenName { get; set; }
	public required List<string> Topics { get; set; }
}
