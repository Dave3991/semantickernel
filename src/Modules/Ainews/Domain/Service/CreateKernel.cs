using Microsoft.SemanticKernel;
using Microsoft.SemanticKernel.Plugins.Core;
using Microsoft.SemanticKernel.Plugins.Memory;
using SemanticKernel.Modules.Ainews.Domain.Enum;
using SemanticKernel.Modules.Ainews.Setup;

namespace SemanticKernel.Modules.Ainews.Domain.Service;

public class CreateKernel
{
    public IKernelBuilder CreateKernelBuilder()
    {
        var builder = Kernel.CreateBuilder();
        // builder.AddOpenAITextEmbeddingGeneration(ModelId.EmbedingModelAda002, Services.GetOpenAiApiKey());
        builder.AddOpenAITextGeneration(ModelId.GPT4o, Services.GetOpenAiApiKey());
        builder.AddOpenAIChatCompletion(ModelId.GPT4o, Services.GetOpenAiApiKey());
        builder.Plugins.AddFromType<TextMemoryPlugin>();
        builder.Plugins.AddFromType<TimePlugin>();
        builder.Plugins.AddFromType<TextPlugin>();
        return builder;
    }

}
