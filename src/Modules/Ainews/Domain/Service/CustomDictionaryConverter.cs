using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text.Json;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

public class CustomDataConverter : JsonConverter
{
    public override bool CanConvert(Type objectType)
    {
        return objectType == typeof(Dictionary<string, object>);
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        var dictionary = new Dictionary<string, object>();
        JObject jo = JObject.Load(reader);

        foreach (var property in jo.Properties())
        {
            dictionary[property.Name] = property.Value.ToObject<object>();
        }

        return dictionary;
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        var dictionary = (Dictionary<string, object>)value;
        writer.WriteStartObject();

        foreach (var kvp in dictionary)
        {
            writer.WritePropertyName(kvp.Key);
            WriteValue(writer, kvp.Value, serializer);
        }

        writer.WriteEndObject();
    }

    private void WriteValue(JsonWriter writer, object value, JsonSerializer serializer)
    {
        if (value is JsonElement jsonElement)
        {
            switch (jsonElement.ValueKind)
            {
                case JsonValueKind.String:
                    writer.WriteValue(jsonElement.GetString());
                    break;
                case JsonValueKind.Number:
                    if (jsonElement.TryGetInt32(out int intValue))
                    {
                        writer.WriteValue(intValue);
                    }
                    else if (jsonElement.TryGetInt64(out long longValue))
                    {
                        writer.WriteValue(longValue);
                    }
                    else if (jsonElement.TryGetDouble(out double doubleValue))
                    {
                        writer.WriteValue(doubleValue);
                    }
                    break;
                case JsonValueKind.True:
                case JsonValueKind.False:
                    writer.WriteValue(jsonElement.GetBoolean());
                    break;
                case JsonValueKind.Null:
                    writer.WriteNull();
                    break;
                default:
                    writer.WriteValue(jsonElement.ToString());
                    break;
            }
        }
        else
        {
            serializer.Serialize(writer, value);
        }
    }
}
