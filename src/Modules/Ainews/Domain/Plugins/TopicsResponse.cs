namespace SemanticKernel.Modules.Ainews.Domain.Plugins;

public class TopicsResponse
{
    public List<string> Topics { get; set; }
}