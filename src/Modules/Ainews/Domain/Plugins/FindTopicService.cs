using Json.More;
using Microsoft.EntityFrameworkCore;
using Microsoft.SemanticKernel;
using Microsoft.SemanticKernel.Connectors.OpenAI;
using SemanticKernel.Modules.Ainews.Domain.Entities;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;
using SemanticKernel.Modules.Ainews.Domain.Enum;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance;
using SemanticKernel.Modules.Ainews.Setup;

namespace SemanticKernel.Modules.Ainews.Domain.Plugins;

public class FindTopicService
{
    
    private readonly AzureDbContext _context;

    public FindTopicService(AzureDbContext context)
    {
        _context = context;
    }


    //use semantic kernel with AI to find topics in the text
    public async Task<FunctionResult?> FindTopic(string text)
    {
        // Initialize the kernel with the necessary AI models
        var kernel = Kernel.CreateBuilder()
            .AddOpenAIChatCompletion(ModelId.GPT4o, Services.GetOpenAiApiKey())
            .Build();
        
        var existingTopics = await _context.Set<TopicEntity>().ToListAsync();
        var existingTopicsStr = string.Join(", ", existingTopics.Select(t => t.Name));
        // Set up arguments for the kernel call
#pragma warning disable SKEXP0010
        KernelArguments arguments = new KernelArguments(new OpenAIPromptExecutionSettings { ResponseFormat = "json_object" });
        arguments.Add("text", text);
        arguments.Add("existingTopicsStr", existingTopicsStr);

        // Use AI to extract topics from the text
        string prompt = $"Identify relevant topics that could serve as metadata for improved searchability from the following text," +
                        $" return topics in JSON: " +
                        $"```{text}```" + 
                        $"Consider existing topics, which follows, in the database to avoid suggesting synonyms or duplicates, existing topics: ```{existingTopicsStr}```";
        try
        {
            var result = await kernel.InvokePromptAsync(prompt, arguments);
            return result;
        }
        catch (Exception ex)
        {
            // Handle exceptions (e.g., API errors, network issues)
            Console.WriteLine($"Error finding topics: {ex.Message}");
            return null;
        }
    }
}
