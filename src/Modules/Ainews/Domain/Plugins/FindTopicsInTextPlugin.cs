using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SemanticKernel.Modules.Ainews.Domain.Entities;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance;

namespace SemanticKernel.Modules.Ainews.Domain.Plugins;

/// <summary>
/// AI plugin finds topics which given text contains
/// </summary>
public class FindTopicsInTextPlugin
{
    private readonly ILogger<FindTopicsInTextPlugin> _logger;
    private readonly AzureDbContext _context;
    
    private FindTopicService _findTopicService;

    public FindTopicsInTextPlugin(AzureDbContext context, FindTopicService findTopicService, ILogger<FindTopicsInTextPlugin> logger)
    {
        _context = context;
        _findTopicService = findTopicService;
        _logger = logger;
    }

    public async Task<List<TopicEntity>> FindTopicsInText(string text)
    {
        // Load existing topics from the database
        var existingTopics = await _context.Set<TopicEntity>().ToListAsync();

        var foundTopicsInTextFuncRes = await _findTopicService.FindTopic(text) ?? throw new Exception($"Error finding topics {text}");

        var foundTopicsJsonString = foundTopicsInTextFuncRes.GetValue<string>();
        
        if (foundTopicsJsonString == null)
        {
            throw new Exception("Error finding topics in the text");
        }
        
        // Parse the result into a TopicsResponse object
        TopicsResponse? foundTopicsResponse;
        try {
	        foundTopicsResponse = JsonConvert.DeserializeObject<TopicsResponse>(foundTopicsJsonString);
		} catch (JsonReaderException ex) {
			_logger.LogError(ex, "Failed to parse the topics response: {JsonString}", foundTopicsJsonString);
			throw;
		}

        var foundTopics = new List<TopicEntity>();

        // Compare the text with the existing topics
        if (foundTopicsResponse?.Topics == null)
        {
            _logger.LogWarning("No topics found in the text");
            return foundTopics;
        }
        foreach (var topicName in foundTopicsResponse.Topics)
        {
            var existingTopic = existingTopics.FirstOrDefault(t => t.Name.ToLower() == topicName.ToLower());
            if (existingTopic != null)
            {
                // If the topic exists in the database, add it to the foundTopics list
                foundTopics.Add(existingTopic);
            }
            else
            {
                // If a topic in the text does not exist in the database, add it
                var newTopic = new TopicEntity { Name = topicName };
                _context.Set<TopicEntity>().Add(newTopic);
                foundTopics.Add(newTopic);
            }
        }
        

        // Save changes to the database
        await _context.SaveChangesAsync();

        // Return the topics found in the text
        return foundTopics;
    }
}
