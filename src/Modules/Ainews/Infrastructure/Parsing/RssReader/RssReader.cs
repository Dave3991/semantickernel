using System.Net.Http;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;
using System.Xml;
using FluentResults;
using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Services;

namespace SemanticKernel.Modules.Ainews.Infrastructure.Parsing.RssReader
{
	public class RssReader
	{
		private readonly SavingArticleService _savingArticleService;
		private readonly HttpClient _httpClient;
		private readonly ILogger<RssReader> _logger;

		public RssReader(SavingArticleService savingArticleService, HttpClient httpClient, ILogger<RssReader> logger)
		{
			_savingArticleService = savingArticleService;
			_httpClient = httpClient;
			_logger = logger;
		}

		public async Task<Result> FetchFeed(string url, List<string> topics)
		{
			int maxRetries = 5;
			int delay = 1000; // Initial delay in milliseconds

			var handler = new HttpClientHandler()
			{
				ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true,
				AllowAutoRedirect = true,
				MaxAutomaticRedirections = 10
			};

			using (var client = new HttpClient(handler) { Timeout = TimeSpan.FromMinutes(3) })
			{
				client.DefaultRequestHeaders.UserAgent.ParseAdd(
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3");
				client.DefaultRequestHeaders.Accept.ParseAdd(
					"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
				client.DefaultRequestHeaders.AcceptLanguage.ParseAdd("en-US,en;q=0.5");
				client.DefaultRequestHeaders.Connection.ParseAdd("keep-alive");
				client.DefaultRequestHeaders.Host = new Uri(url).Host;

				for (int retry = 0; retry < maxRetries; retry++)
				{
					try
					{
						_logger.LogInformation("Fetching RSS feed from {Url}", url);
						var response = await client.GetAsync(url);

						// log response
						_logger.LogInformation("Response: {Response}", response);

						var stream = await response.Content.ReadAsStreamAsync();

						using (var reader = XmlReader.Create(stream))
						{
							var feed = SyndicationFeed.Load(reader);
							foreach (var item in feed.Items)
							{
								var itemContent = item.Content as TextSyndicationContent;
								var article = new ArticleEntity
								{
									Title = item.Title.Text,
									ArticleUrl = item.Links[0].Uri.AbsoluteUri,
									ImageUrl = item.Links[0].Uri.AbsoluteUri,
									Content = itemContent.Text,
									PublishDate = item.PublishDate.DateTime.ToUniversalTime(),
									Sources = new List<string> { url }
								};

								var elasticArticle = new ElasticArticleEntity
								{
									Title = item.Title.Text,
									ArticleUrl = item.Links[0].Uri.AbsoluteUri,
									ImageUrl = item.Links[0].Uri.AbsoluteUri,
									Content =  itemContent.Text,
									PublishDate = item.PublishDate.DateTime.ToUniversalTime(),
									Sources = new List<string> { url }
								};

								var saveResult = await _savingArticleService.SaveArticleAsync(article, elasticArticle);
								if (saveResult.IsSuccess)
								{
									return Result.Ok();
								}
								else
								{
									return Result.Fail("Failed to save articles");
								}
							}
						}
					}
					catch (HttpRequestException ex)
					{
						_logger.LogError(ex, "Error fetching RSS feed from {Url}. Attempt {Retry}/{MaxRetries}", url,
							retry + 1, maxRetries);
						if (retry == maxRetries - 1)
						{
							return Result.Fail(new List<string> { ex.Message });
						}
					}

					await Task.Delay(delay);
					delay *= 2; // Exponential backoff
				}
			}

			return Result.Fail(new List<string> { "Max retry attempts exceeded." });
		}

		public async Task<FluentResults.Result<string>> LoadDescription(string url, string source, List<string> topics)
		{
			var handler = new HttpClientHandler()
			{
				ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true,
				AllowAutoRedirect = true,
				MaxAutomaticRedirections = 10
			};

			var client = new HttpClient(handler) { Timeout = TimeSpan.FromMinutes(1) };

			client.DefaultRequestHeaders.UserAgent.ParseAdd(
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3");
			client.DefaultRequestHeaders.Accept.ParseAdd(
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			client.DefaultRequestHeaders.AcceptLanguage.ParseAdd("en-US,en;q=0.5");
			client.DefaultRequestHeaders.Connection.ParseAdd("keep-alive");
			client.DefaultRequestHeaders.Host = new Uri(url).Host;

			var retries = 3;
			for (var i = 0; i < retries; i++)
			{
				try
				{
					var result = await HandleWebPageSource(client, url, source, topics);
					if (result.IsSuccess)
					{
						return result.Value;
					}

					_logger.LogWarning("Attempt {Attempt} failed: {Error}", i + 1, result.Errors);
				}
				catch (Exception ex)
				{
					_logger.LogWarning("Attempt {Attempt} threw an exception: {Exception}", i + 1, ex);
				}
			}

			return FluentResults.Result.Fail<string>("Failed to load description after retries");
		}

		private async Task<FluentResults.Result<string>> HandleWebPageSource(HttpClient client, string url,
			string source, List<string> topics)
		{
			var response = await client.GetAsync(url);
			var content = await response.Content.ReadAsStringAsync();
			var doc = new HtmlDocument();
			doc.LoadHtml(content);
			var description = doc.DocumentNode.SelectSingleNode("//meta[@name='description']");

			if (description != null && description.Attributes.Contains("content"))
			{
				return FluentResults.Result.Ok(description.Attributes["content"].Value);
			}

			return FluentResults.Result.Fail<string>("Description not found");
		}
	}
}
