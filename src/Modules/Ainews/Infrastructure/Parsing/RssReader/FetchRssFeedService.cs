using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SemanticKernel.Modules.Ainews.Infrastructure.Parsing.RssReader
{
	public class FetchRssFeedService : BackgroundService
	{
		private readonly RssReader _rssReader;
		private readonly ILogger<FetchRssFeedService> _logger;

		public FetchRssFeedService(RssReader rssReader, ILogger<FetchRssFeedService> logger)
		{
			_rssReader = rssReader;
			_logger = logger;
		}

		protected override async Task ExecuteAsync(CancellationToken stoppingToken)
		{
			var urlsToFetch = new Dictionary<string, List<string>>
			{
				// {"https://www.nasdaq.com/feed/nasdaq-original/rss.xml", new List<string> { "stocks" }},
				//{"https://www.nasdaq.com/feed/rssoutbound?category=Nasdaq", new List<string> { "stocks" }},
				{"https://feeds.feedburner.com/brontecapital", new List<string> { "stocks" }},
			};

			if (Environment.GetEnvironmentVariable("ENABLED_SCRAPING") == "true" &&
			    Environment.GetEnvironmentVariable("ENABLE_SCRAPING_RSS") == "true")
			{
				_logger.LogInformation("Running background task for fetching RSS feeds");

				while (!stoppingToken.IsCancellationRequested)
				{
					foreach (var url in urlsToFetch)
					{
						var result = await _rssReader.FetchFeed(url.Key, url.Value);
						if (result.IsFailed)
						{
							_logger.LogError("Failed to fetch RSS feed with error: {Join}", string.Join(", ", result.Errors));
						}
					}

					await Task.Delay(5 * 60 * 1000, stoppingToken);
				}
			}
		}
	}
}
