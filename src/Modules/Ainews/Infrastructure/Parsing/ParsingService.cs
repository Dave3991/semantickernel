using FluentResults;
using SemanticKernel.Modules.Ainews.Domain.Case.ScrapeTwitterCase;
using SemanticKernel.Modules.Ainews.Domain.DTOs;
using SemanticKernel.Modules.Ainews.Domain.Entities.TwitterApi;

namespace SemanticKernel.Modules.Ainews.Infrastructure.Parsing;

public class ParsingService : BackgroundService
{
    private readonly ILogger<ParsingService> _logger;
    private ScrapeTwitterCase _scrapeTwitterCase;

    public ParsingService(ILogger<ParsingService> logger, ScrapeTwitterCase scrapeTwitterCase)
    {
        _logger = logger;
        _scrapeTwitterCase = scrapeTwitterCase;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
	    while (!stoppingToken.IsCancellationRequested)
	    {
		    
		    // Define the list of screen names and topics
		    var scrapeTargets = new List<(string ScreenName, List<string> Topics)>
		    {
			    ("visegrad24", new List<string> { "news" }),
			    ("nytimes", new List<string> { "news" }),
			    ("nytimesworld", new List<string> { "news", "world news" }),
			    ("BBCBreaking", new List<string> { "news" }),
			    ("washingtonpost", new List<string> { "news" }),
			    ("CNN", new List<string> { "news" }),
			    ("YahooFinance", new List<string> { "news", "Finance" }),
			    ("BBCBusiness", new List<string> { "news", "Finance" }),
			    ("ReutersBiz", new List<string> { "news", "Finance" }),
			    ("WSJecon", new List<string> { "news", "Finance" }),
			    ("PelosiTracker_", new List<string> { "Stocks" }),
			    ("unusual_whales", new List<string> { "Finance", "Stocks" }),
			    ("financialjuice", new List<string> { "Finance", "Stocks" }),
			    ("greenstocknews", new List<string> { "Finance", "Stocks" }),
			    ("Fxhedgers", new List<string> { "Finance", "Stocks" }),
			    ("arstechnica", new List<string> { "technology", "news" }),
			    
			    // technology
			    ("technology", new List<string> { "technology" }),
			    ("TechCrunch", new List<string> { "technology" }),
			    ("fttechnews", new List<string> { "technology" }),
			    ("CNBCtech", new List<string> { "technology" }),
			    ("SeekingAlpha", new List<string> { "technology" }),
			    ("ExtremeTech", new List<string> { "technology" }),
			    ("VentureBeat", new List<string> { "technology" }),
		    };

		    // Execute the scraping for each target in parallel
		    //if env is dev, dont scrape
		    if (Environment.GetEnvironmentVariable("ENABLED_SCRAPING") == "true" && Environment.GetEnvironmentVariable("ENABLE_SCRAPING_TWITTER") == "true")
		    {
			    _logger.LogInformation("Running background task for scraping Twitter");
			    
			    var tasks = scrapeTargets.Select(target => ExecuteScrapeAsync(target.ScreenName, target.Topics));
			    var results = new List<Result<TweetJsonResponse>>();
			    const int maxThreads = 1;

			    for (int i = 0; i < tasks.Count(); i += maxThreads)
			    {
				    var chunk = tasks.Skip(i).Take(maxThreads);
				    var chunkResults = await Task.WhenAll(chunk);
				    results.AddRange(chunkResults);
				    await Task.Delay(1000);
			    }
		    
			    // Log any failures
			    foreach (var result in results)
			    {
				    if (result.IsFailed)
				    {
					    _logger.LogError("Scraping failed with error: {Join}", string.Join(", ", result.Errors));
					    // if error is 429, then we have reached the rate limit, increase the delay
				    
				    }
			    }
		    }
		   

		    // await for 5 minutes
		    await Task.Delay(15 * 60 * 1000, stoppingToken);
	    }
    }

    private async Task<Result<TweetJsonResponse>> ExecuteScrapeAsync(string screenName, List<string> topics)
    {
        return await _scrapeTwitterCase.ExecuteAsync(new ScrapeTwitterCaseDto()
        {
            ScreenName = screenName,
            Topics = topics,
        });
    }
}
