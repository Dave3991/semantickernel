using FluentResults;
using SemanticKernel.Modules.Ainews.Domain.Case.ScrapeRedditCase;

namespace SemanticKernel.Modules.Ainews.Infrastructure.Parsing;

public class ParsingRedditService: BackgroundService
{
	
	
	private readonly ILogger<ParsingRedditService> _logger;
	private IServiceScopeFactory _serviceScopeFactory;

	public ParsingRedditService(ILogger<ParsingRedditService> logger, IServiceScopeFactory serviceProvider)
	{
		_logger = logger;
		_serviceScopeFactory = serviceProvider;
	}

	protected override async Task ExecuteAsync(CancellationToken stoppingToken)
	{
		while (!stoppingToken.IsCancellationRequested)
		{
			using var scope = _serviceScopeFactory.CreateScope();
			var _scrapeRedditCase = scope.ServiceProvider.GetRequiredService<ScrapeRedditCase>();
			
			//if env is dev, dont scrape
			if (Environment.GetEnvironmentVariable("ENABLED_SCRAPING") == "true" && Environment.GetEnvironmentVariable("ENABLE_SCRAPING_REDDIT") == "true")
			{
				_logger.LogInformation("Running background task for scraping Reddit");
				await _scrapeRedditCase.Execute();
				_logger.LogInformation("Finished scraping Reddit");
			}
			
			
			
			// wait 5 minutes before scraping again
			await Task.Delay(5 * 60 * 1000, stoppingToken);
		}
	}
}
