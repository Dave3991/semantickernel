using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;
using ILogger = SharpYaml.Serialization.Logging.ILogger;

namespace SemanticKernel.Modules.Ainews.Infrastructure.Parsing;

/**
 * This class is responsible for deleting articles older than 4 days.
 */
public class DeleteOldArticles: BackgroundService
{
	private readonly ILogger<DeleteOldArticles> _logger;
	private readonly DeleteOldArticlesOperation _deleteOperation;

	public DeleteOldArticles(ILogger<DeleteOldArticles> logger, DeleteOldArticlesOperation deleteOperation)
	{
		_logger = logger;
		_deleteOperation = deleteOperation;
	}

	protected override async Task ExecuteAsync(CancellationToken stoppingToken)
	{
		while (!stoppingToken.IsCancellationRequested)
		{
			_logger.LogInformation("Running background task for deleting old articles");

			// Define the date 4 days ago
			// var olderThan = DateTime.Now.AddDays(-4);
			//
			// // Delete articles older than 4 days
			// var result = _deleteOperation.DeleteOldArticlesAsync(olderThan).Result;
			//
			// if (result.IsFailed)
			// {
			// 	_logger.LogError("Failed to delete old articles with error: {Join}", string.Join(", ", result.Errors));
			// }

			// await for 1 day
			await Task.Delay(24 * 60 * 60 * 1000, stoppingToken);
		}
	}
}
