using FluentResults;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using SemanticKernel.Modules.Ainews.Domain.Entities;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance;

namespace SemanticKernel.Modules.Ainews.Infrastructure.Persistance.PostgreSQL.Operation;

public class SaveArticleTopicEntityOperation
{
	private readonly IDbContextFactory<AzureDbContext> _contextFactory;
	private readonly ILogger<SaveArticleTopicEntityOperation> _logger;
	private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

	public SaveArticleTopicEntityOperation(IDbContextFactory<AzureDbContext> contextFactory,
		ILogger<SaveArticleTopicEntityOperation> logger)
	{
		_contextFactory = contextFactory;
		_logger = logger;
	}

	public async Task<Result<string>> Save(ArticleEntity article)
	{
		if (string.IsNullOrEmpty(article.Title))
		{
			_logger.LogError("Article title cannot be null or empty {StackTrace}", Environment.StackTrace);
			return Result.Fail("Article title cannot be null or empty");
		}
		await _semaphore.WaitAsync();
		{
			try
			{
				// Create a new DbContext instance for this operation
				await using var context = await _contextFactory.CreateDbContextAsync();

				// Check if an article with the same source already exists
				var existingArticle = await context.ArticleEntity.FirstOrDefaultAsync(a => a.ArticleUrl == article.ArticleUrl);
				if (existingArticle != null)
				{
					_logger.LogWarning("Article with the source {ArticleSource} already exists", article.ArticleUrl);
					return FluentResults.Result.Fail<string>("Article with the same source already exists");
				}

				FluentResults.Result saveResult;
				try
				{
					// Add the ArticleEntity to the context
					context.ArticleEntity.Add(article);

					// Save changes to the context
					await context.SaveChangesAsync();

					saveResult = FluentResults.Result.Ok();
				}
				catch (DbUpdateException ex) when (ex.InnerException is PostgresException pgEx &&
				                                   pgEx.SqlState == "23505")
				{
					_logger.LogError(ex, "Failed to save Article with the source {ArticleSource} due to duplicate key",
						article.ArticleUrl);
					saveResult = FluentResults.Result.Fail("Failed to save Article due to duplicate key")
						.WithError(ex.Message);
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, "Failed to save Article with the source {ArticleSource}", article.ArticleUrl);
					saveResult = FluentResults.Result.Fail("Failed to save Article").WithError(ex.Message);
				}

				if (saveResult.IsFailed)
				{
					_logger.LogError("Failed to save Article with the source {ArticleSource}: {Error}", article.ArticleUrl,
						saveResult.Errors);
					return FluentResults.Result.Fail<string>("Failed to save Article");
				}

				_logger.LogInformation("Article with the source {ArticleSource} saved successfully", article.ArticleUrl);

				// Return the Id of the new ArticleEntity as a string
				return FluentResults.Result.Ok(article.Id.ToString());
			}
			finally
			{
				_semaphore.Release();
			}
		}
	}
}
