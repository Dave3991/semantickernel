using FluentResults;
using Microsoft.EntityFrameworkCore;
using SemanticKernel.Modules.Ainews.Domain.Enum;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance;
using SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.DTOs;

namespace SemanticKernel.Modules.Ainews.Infrastructure.Persistance.PostgreSQL.Operation;

public class GetArticleTopicsOperation
{
	private readonly IDbContextFactory<AzureDbContext> _contextFactory;

	public GetArticleTopicsOperation(IDbContextFactory<AzureDbContext> contextFactory)
	{
		_contextFactory = contextFactory;
	}

	public List<TopicsDto> GetEnabledArticleTopics()
	{
		return _contextFactory.CreateDbContextAsync().Result.TopicEntity
			.Where(x => x.IsEnabled == true)
			.Select(x => new TopicsDto
			{
				Name = x.Name,
			})
			.ToList();
	}
	
	public async Task<Result<List<TopicsDto>>> GetTopTopicsAsync()
	{
		try
		{
			var topics = await _contextFactory.CreateDbContextAsync().Result.TopicEntity
				.Where(x => x.IsEnabled == true)
				.Where(x => x.Type == TopicNodeTypesEnum.Category)
				.Select(x => new TopicsDto
				{
					Name = x.Name,
				})
				.Take(10)
				.ToListAsync();
			return Result.Ok(topics);
		}
		catch (Exception ex)
		{
			return Result.Fail<List<TopicsDto>>($"Unable to get topics, {System.Reflection.MethodBase.GetCurrentMethod()?.Name}").WithError(ex.Message);
		}
	}
}
