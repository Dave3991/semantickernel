using Microsoft.EntityFrameworkCore;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;
using SemanticKernel.Modules.Ainews.Domain.Enum;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance;

namespace SemanticKernel.Modules.Ainews.Infrastructure.Persistance.PostgreSQL.Operation
{
	public class AddNewNodeOperation
	{
		private readonly AzureDbContext _context;
		private readonly ILogger<AddNewNodeOperation> _logger;

		public AddNewNodeOperation(AzureDbContext context, ILogger<AddNewNodeOperation> logger)
		{
			_context = context;
			_logger = logger;
		}

		public async Task AddNewNodeAsync(string nodeName, TopicNodeTypesEnum nodeType, string parentName)
		{
			// Check if a node with the same name already exists
			var existingNode = await _context.TopicEntity.FirstOrDefaultAsync(n => n.Name == nodeName);
			if (existingNode != null)
			{
				_logger.LogWarning("Node with the name {NodeName} already exists", nodeName);
				return;
			}

			// Retrieve the parent node
			var parentNode = await _context.TopicEntity.FirstOrDefaultAsync(n => n.Name == parentName);
			if (parentNode == null)
			{
				_logger.LogWarning("Parent node with the name {ParentName} does not exist", parentName);
				return;
			}

			// Create a new node
			var newNode = new TopicEntity
			{
				Name = nodeName,
				ParentId = parentNode.Id,
				Type = nodeType
			};

			// Add the TopicNodeEntity to the context
			_context.TopicEntity.Add(newNode);

			// Save changes to the context
			await _context.SaveChangesAsync();
		}
	}
}
