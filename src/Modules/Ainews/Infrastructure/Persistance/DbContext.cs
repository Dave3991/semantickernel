using Microsoft.EntityFrameworkCore;
using SemanticKernel.Modules.Ainews.Domain.Entities;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;
using SemanticKernel.Modules.Ainews.Domain.Enum;

namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance;

public class AzureDbContext: DbContext
{
    public AzureDbContext(DbContextOptions<AzureDbContext> options) : base(options)
    {
    }
    
    private const string Schema = "ainews";
    public DbSet<ArticleEntity> ArticleEntity { get; set; }
    public DbSet<TopicEntity> TopicEntity { get; set; }
    public DbSet<ArticleTopicEntity> ArticleTopicEntity { get; set; }
    public DbSet<UserEntity> UserEntity { get; set; }
    public DbSet<CollectionEntity> CollectionEntity { get; set; }
    public DbSet<TopicCollectionEntity> TopicCollectionEntity { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ArticleEntity>()
            .ToTable("articles", schema: Schema)
            .HasIndex(s =>  s.ContentHash)
            .IsUnique();

        modelBuilder.Entity<TopicEntity>()
            .ToTable("topics", schema: Schema)
            .HasMany(e => e.Children)
            .WithOne(e => e.Parent)
            .HasForeignKey(e => e.ParentId)
            .OnDelete(DeleteBehavior.Restrict);
        
        modelBuilder.Entity<ArticleTopicEntity>()
            .ToTable("article_topics", schema: Schema)
            .HasKey(at => new { at.ArticleId, at.TopicID });

        modelBuilder.Entity<ArticleTopicEntity>()
            .ToTable("article_topics", schema: Schema)
            .HasOne(at => at.Article)
            .WithMany(a => a.ArticleTopics)
            .HasForeignKey(at => at.ArticleId);

        modelBuilder.Entity<ArticleTopicEntity>()
            .ToTable("article_topics", schema: Schema)
            .HasOne(at => at.Topic)
            .WithMany(t => t.ArticleTopics)
            .HasForeignKey(at => at.TopicID);

        modelBuilder.Entity<UserEntity>()
            .ToTable("users", schema: Schema);

        modelBuilder.Entity<CollectionEntity>()
            .ToTable("collections", schema: Schema)
            .HasOne(c => c.User)
            .WithMany(u => u.Collections)
            .HasForeignKey(c => c.UserId);

        modelBuilder.Entity<TopicCollectionEntity>()
            .ToTable("topic_collections", schema: Schema)
            .HasKey(tc => new { tc.CollectionId, tc.TopicId });

        modelBuilder.Entity<TopicCollectionEntity>()
            .ToTable("topic_collections", schema: Schema)
            .HasOne(tc => tc.Collection)
            .WithMany(c => c.TopicCollections)
            .HasForeignKey(tc => tc.CollectionId);

        modelBuilder.Entity<TopicCollectionEntity>()
            .ToTable("topic_collections", schema: Schema)
            .HasOne(tc => tc.Topic)
            .WithMany(t => t.TopicCollections)
            .HasForeignKey(tc => tc.TopicId);
        
        // Seed initial data for root nodes
        SeedRootNodes(modelBuilder);
    }

    private void SeedRootNodes(ModelBuilder modelBuilder)
    {
	    modelBuilder.Entity<TopicEntity>().HasData(
		    new TopicEntity { Id = 1, Name = "Root", ParentId = null, Type = TopicNodeTypesEnum.Category }
	    );
    }
}
