using System.Net;
using FluentResults;
using HtmlAgilityPack;

namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance.Services;

public class ContentService
{
	private readonly ILogger<ContentService> _logger;

	public ContentService(ILogger<ContentService> logger)
	{
		_logger = logger;
	}

	public async Task<Result<string>> FetchContentAsync(string url)
    {
        try
        {
            // Use HttpClientHandler to manage cookies
            var handler = new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate,
                UseCookies = true,
                CookieContainer = new CookieContainer(),
                AllowAutoRedirect = true,
                Proxy = null,
                UseProxy = false
            };

            using (HttpClient client = new HttpClient(handler))
            {
                // Setup client to look like a browser
                client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3");
                client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
                client.DefaultRequestHeaders.Add("Accept-Language", "en-US,en;q=0.5");
                client.DefaultRequestHeaders.Add("Connection", "keep-alive");
                client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                client.DefaultRequestHeaders.Add("Cache-Control", "max-age=0");
                
                // Add headers that mimic a real browser's request
                client.DefaultRequestHeaders.Add("DNT", "1"); // Do Not Track
                client.DefaultRequestHeaders.Add("Referer", url); // Referer header
                
                client.Timeout = TimeSpan.FromSeconds(30); // Adjust timeout as needed

                // Fetch the HTML content from the URL
                HttpResponseMessage response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                string html = await response.Content.ReadAsStringAsync();

                // Handle Meta Refresh Redirects
                var redirectUrl = GetMetaRefreshUrl(html);
                if (redirectUrl != null)
                {
	                return await FetchContentAsync(redirectUrl);
                }

                // Handle JavaScript Redirects
                redirectUrl = GetJavaScriptRedirectUrl(html);
                if (redirectUrl != null)
                {
	                return await FetchContentAsync(redirectUrl);
                }

                // Load the HTML into HtmlAgilityPack
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(html);

                // Extract the content or description
                HtmlNode metaDescription = doc.DocumentNode.SelectSingleNode("//meta[@name='description']");
                if (metaDescription != null)
                {
                    return Result.Ok(metaDescription.GetAttributeValue("content", string.Empty));
                }

                var bodyNode = doc.DocumentNode.SelectSingleNode("//body");
                if (bodyNode != null)
                {
                    return Result.Ok(bodyNode.InnerText);
                }

                return Result.Fail($"No content found for {url}");
            }
        }
        catch (Exception ex)
        {
            return Result.Fail($"Error fetching content").WithError(ex.Message + ex.StackTrace);
        }
    }
	
	private string GetMetaRefreshUrl(string html)
	{
		HtmlDocument doc = new HtmlDocument();
		doc.LoadHtml(html);
		HtmlNode metaRefresh = doc.DocumentNode.SelectSingleNode("//meta[@http-equiv='refresh']");
		if (metaRefresh != null)
		{
			string content = metaRefresh.GetAttributeValue("content", string.Empty);
			var parts = content.Split(';');
			foreach (var part in parts)
			{
				if (part.Trim().ToLower().StartsWith("url="))
				{
					return part.Substring(4).Trim();
				}
			}
		}
		return null;
	}

	private string GetJavaScriptRedirectUrl(string html)
	{
		var doc = new HtmlDocument();
		doc.LoadHtml(html);
		var scriptNodes = doc.DocumentNode.SelectNodes("//script");
		if (scriptNodes != null)
		{
			foreach (var script in scriptNodes)
			{
				string scriptContent = script.InnerHtml;
				if (scriptContent.Contains("location.replace"))
				{
					int startIndex = scriptContent.IndexOf("location.replace") + "location.replace".Length;
					startIndex = scriptContent.IndexOf("(", startIndex) + 1;
					int endIndex = scriptContent.IndexOf(")", startIndex);
					string url = scriptContent.Substring(startIndex, endIndex - startIndex).Replace("\"", "").Replace("'", "").Trim();
					return url;
				}
			}
		}
		return null;
	}
}
