using FluentResults;
using Newtonsoft.Json;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.Persistance.PostgreSQL.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.Operation;

namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance.Services;

public class SavingArticleService
{
	private readonly SaveArticleOperation _saveArticleOperation;
	private readonly SaveArticleTopicEntityOperation _saveArticleTopicEntityOperation;
	private readonly GenerateOperation _generateOperation;
	private readonly ILogger<SaveArticleOperation> _logger;

	public SavingArticleService(SaveArticleOperation saveArticleOperation, SaveArticleTopicEntityOperation saveArticleTopicEntityOperation, GenerateOperation generateOperation, ILogger<SaveArticleOperation> logger)
	{
		_saveArticleOperation = saveArticleOperation;
		_saveArticleTopicEntityOperation = saveArticleTopicEntityOperation;
		_generateOperation = generateOperation;
		_logger = logger;
	}
	
	public async Task<Result> SaveArticleAsync(ArticleEntity article, ElasticArticleEntity elasticArticleEntity)
	{
		// Ensure Topics is not null
		elasticArticleEntity.Topics = elasticArticleEntity.Topics ?? new List<string>();
		// Save the article to the database, check if there are any duplicates
		var saveResult = new Result<string>();
		try
		{
			saveResult = await _saveArticleTopicEntityOperation.Save(article);
			if (saveResult.IsFailed)
			{
				return Result.Fail($"Failed to save article {article.ArticleUrl} to database");
			}
		} catch (Exception ex)
		{
			_logger.LogError(ex, "Failed to save article {ArticleUrl} to database", article.ArticleUrl);
			return Result.Fail($"Failed to save article {article.ArticleUrl} to database").WithError(ex.Message);
		}

		// Generate possible topics
		var topics = await _generateOperation.GetTopicFromText(elasticArticleEntity.Content);
		if (topics.IsFailed)
		{
			return topics.ToResult().WithErrors(topics.Errors);
		}
		
		if (topics.Value.Topics.Count == 0)
		{
			_logger.LogWarning("No topics found for article {ArticleUrl}", article.ArticleUrl);
		}
		
		try
		{
			// Add topics to the article
			elasticArticleEntity.Topics.AddRange(topics.Value.Topics);
			
			// Remove duplicates from the topics ignoring case
			elasticArticleEntity.Topics = elasticArticleEntity.Topics.Distinct(StringComparer.OrdinalIgnoreCase).ToList();
			
			// Add extra information to the article
			elasticArticleEntity.CustomData.Add("value_assessment", topics.Value.ValueAssessment);

			// Save to elastic
			var saveArticleResult = await _saveArticleOperation.SaveArticleAsync(elasticArticleEntity);
			if (saveArticleResult.IsFailed)
			{
				return saveArticleResult;
			}

			// Update the article with the ElasticArticleId
			article.ElasticArticleId = elasticArticleEntity.Id;
			saveResult = await _saveArticleTopicEntityOperation.Save(article);
			if (saveResult.IsFailed)
			{
				_logger.LogError(saveResult.Errors.First().Message);
				return Result.Fail("Failed to save article to database").WithErrors(saveResult.Errors);
			}
		}
		catch (Exception ex)
		{
			_logger.LogError(ex, "Failed to save article");
			return Result.Fail(ex.Message + " " + ex.StackTrace + " " + ex.InnerException?.Message + " " + ex.InnerException?.StackTrace + "Article:" + article.ElasticArticleId + " " + article.ArticleUrl);
		}

		return Result.Ok();
	}
	
}
