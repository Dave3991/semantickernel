using System.Text.RegularExpressions;
using Elastic.Clients.Elasticsearch;
using Elastic.Clients.Elasticsearch.QueryDsl;
using FluentResults;
using OpenQA.Selenium.DevTools.V85.CSS;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.DTOs;
using Result = FluentResults.Result;

namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;

public class GetArticleSourcesOperation
{
	private readonly ElasticsearchClient _client;
	private readonly ILogger<GetArticleSourcesOperation> _logger;
	
	public GetArticleSourcesOperation(ElasticConnector connector, ILogger<GetArticleSourcesOperation> logger)
	{
		_client = connector.GetClient();
		_logger = logger;
	}
	
	public async Task<Result<ArticleSourcesDto>> GetArticleSourcesAsync(string query)
	{
		var searchRequest = new SearchRequest<ElasticArticleEntity>
		{
			Query = new WildcardQuery(new Field("sources"))
			{
				Value = query,
				CaseInsensitive = true
			}
		};

		var response = await _client.SearchAsync<ElasticArticleEntity>(searchRequest);
		
		if (response.IsValidResponse)
		{
			var sources = response.Documents.SelectMany(x => x.Sources).Distinct().ToList();
			
			//remove any wildcards symbols from query
			var linqQuery = query.Replace("*", "");
			
			// do wildcard search on sources using LINQ and return ArticleSourcesDto
			var sourcesFiltered = from source in sources
				where source.Contains(linqQuery)
				select source;
			
			var result = new ArticleSourcesDto
			{
				Sources = sourcesFiltered.ToList()
			};
			
			return FluentResults.Result.Ok(result);
		}
		else
		{
			var error = response.ElasticsearchServerError != null
				? response.ElasticsearchServerError.Error.ToString()
				: "Unknown error";
			var message = $"Failed to get ArticleSourcesDto: {error}";
			_logger.LogError(message);
			return FluentResults.Result.Fail<ArticleSourcesDto>(message);
		}
	}
}
