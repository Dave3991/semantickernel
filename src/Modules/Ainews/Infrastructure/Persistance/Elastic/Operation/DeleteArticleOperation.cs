using Elastic.Clients.Elasticsearch;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;

namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;

public class DeleteArticleOperation
{
	private readonly ElasticsearchClient _client;
	
	private readonly ILogger<DeleteArticleOperation> _logger;
	
	public DeleteArticleOperation(ElasticConnector connector, ILogger<DeleteArticleOperation> logger)
	{
		_logger = logger;
		_client = connector.GetClient();
	}
	
	public async Task<FluentResults.Result<string>> DeleteArticleAsync(string id)
	{
		var deleteRequest = new DeleteRequestDescriptor<ElasticArticleEntity>(id);
		var response = await _client.DeleteAsync<ElasticArticleEntity>(deleteRequest);
		
		if (response.IsValidResponse)
		{
			return FluentResults.Result.Ok();
		}
		else
		{
			var error = response.ElasticsearchServerError != null
				? response.ElasticsearchServerError.Error.ToString()
				: "Unknown error";
			var message = $"Failed to delete Article: {error}";
			_logger.LogError(message);
			return FluentResults.Result.Fail(message);
		}
	}
}
