using Elastic.Clients.Elasticsearch;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;

namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;

/// <summary>
/// Class saves Article to Elastic Search
/// </summary>
public class SaveArticleOperation
{
	private readonly ElasticsearchClient _elasticClient;
	private readonly ILogger<SaveArticleOperation> _logger;

	public SaveArticleOperation(ElasticConnector elasticConnector, ILogger<SaveArticleOperation> logger)
	{
		_logger = logger;
		_elasticClient = elasticConnector.GetClient();
	}

	public async Task<FluentResults.Result> SaveArticleAsync(ElasticArticleEntity article)
	{
		try
		{
			// Index the ElasticArticleEntity
			var indexResponse = await _elasticClient.IndexAsync(article);

			var updateResponse = await _elasticClient.UpdateAsync<ElasticArticleEntity, ElasticArticleEntity>(article.Id,
				u => u.Doc(article).Upsert(article));

			if (!updateResponse.IsValidResponse)
			{
				var error = updateResponse.ElasticsearchServerError != null
					? updateResponse.ElasticsearchServerError.Error.ToString()
					: "Unknown error";
				var elasticsearchWarnings = updateResponse.ElasticsearchWarnings;
				var message = $"Failed to update ElasticArticleEntity: {error} Warnings: {elasticsearchWarnings}";
				_logger.LogError("{Message}",message);
				return FluentResults.Result.Fail(message);
			}
			// Check if the indexing operation was successful
			// 	
			if (!indexResponse.IsValidResponse)
			{
				var error = indexResponse.ElasticsearchServerError != null
					? indexResponse.ElasticsearchServerError.Error.ToString()
					: "Unknown error";
				var elasticsearchWarnings = indexResponse.ElasticsearchWarnings;
				var message = $"Failed to index ElasticArticleEntity: {error} Warnings: {elasticsearchWarnings}";
				_logger.LogError("{Message}",message);
				return FluentResults.Result.Fail(message);
			}
		}
		catch (IOException ex)
		{
			_logger.LogError(ex, "Failed to index ElasticArticleEntity due to a network error");
			return FluentResults.Result.Fail(ex.Message);
		}
		catch (Exception ex)
		{
			_logger.LogError(ex, "Failed to index ElasticArticleEntity due to an unexpected error");
			return FluentResults.Result.Fail(ex.Message);
		}
		
		
		
		return FluentResults.Result.Ok();
	}
	
}
