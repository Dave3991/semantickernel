using Elastic.Clients.Elasticsearch;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;

namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;

public class DeleteOldArticlesOperation
{
	private readonly ElasticsearchClient _client;

	private readonly ILogger<DeleteOldArticlesOperation> _logger;

	public DeleteOldArticlesOperation(ElasticConnector connector, ILogger<DeleteOldArticlesOperation> logger)
	{
		_client = connector.GetClient();
		_logger = logger;
	}

	public async Task<FluentResults.Result<string>> DeleteOldArticlesAsync(DateTime olderThan)
	{
		var dateRange = olderThan.ToString("yyyy-MM-dd");
		var searchRequest = new SearchRequestDescriptor<ElasticArticleEntity>()
			.Query(q => q.Range(r => r.DateRange(f => f.Field(a => a.PublishDate).To(dateRange))));
		var response = await _client.SearchAsync<ElasticArticleEntity>(searchRequest);

		if (response.IsValidResponse)
		{
			var tasks = response.Documents.Select(doc => DeleteArticleAsync(doc.Id));
			var results = await Task.WhenAll(tasks);
			var failedResults = results.Where(r => r.IsFailed).ToList();
			if (failedResults.Any())
			{
				var message = $"Failed to delete {failedResults.Count} articles";
				_logger.LogError(message);
				return FluentResults.Result.Fail(message);
			}
			else
			{
				return FluentResults.Result.Ok();
			}
		}
		else
		{
			var error = response.ElasticsearchServerError != null
				? response.ElasticsearchServerError.Error.ToString()
				: "Unknown error";
			var message = $"Failed to search for articles: {error}";
			_logger.LogError(message);
			return FluentResults.Result.Fail(message);
		}
	}
	
	private async Task<FluentResults.Result<string>> DeleteArticleAsync(Guid id)
	{
		var deleteRequest = new DeleteRequestDescriptor<ElasticArticleEntity>(id);
		var response = await _client.DeleteAsync<ElasticArticleEntity>(deleteRequest);
		
		if (response.IsValidResponse)
		{
			return FluentResults.Result.Ok();
		}
		else
		{
			var error = response.ElasticsearchServerError != null
				? response.ElasticsearchServerError.Error.ToString()
				: "Unknown error";
			var message = $"Failed to delete Article: {error}";
			_logger.LogError(message);
			return FluentResults.Result.Fail(message);
		}
	}
}
