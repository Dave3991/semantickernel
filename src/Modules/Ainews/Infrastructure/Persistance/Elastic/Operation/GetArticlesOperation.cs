using Elastic.Clients.Elasticsearch;
using Elastic.Clients.Elasticsearch.QueryDsl;
using FluentResults;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;
using BoolQuery = Elastic.Clients.Elasticsearch.QueryDsl.BoolQuery;
using DateRangeQuery = Elastic.Clients.Elasticsearch.QueryDsl.DateRangeQuery;
using Field = Elastic.Clients.Elasticsearch.Field;
using Fields = Elastic.Clients.Elasticsearch.Fields;
using FieldSort = Elastic.Clients.Elasticsearch.FieldSort;
using Fuzziness = Elastic.Clients.Elasticsearch.Fuzziness;
using MultiMatchQuery = Elastic.Clients.Elasticsearch.QueryDsl.MultiMatchQuery;
using SortOrder = Elastic.Clients.Elasticsearch.SortOrder;

namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation
{
	public class GetArticlesOperation
	{
		private readonly ElasticsearchClient _client;
		private readonly ILogger<GetArticlesOperation> _logger;

		public GetArticlesOperation(ElasticConnector connector, ILogger<GetArticlesOperation> logger)
		{
			_client = connector.GetClient();
			_logger = logger;
		}

		public async Task<Result<List<ElasticArticleEntity>>> GetArticlessAsync(Fields? fields, string query, int pageIndex, int pageSize, DateTime fromDateTime, DateTime toDateTime, List<SortOptions> sortOptions = null)
		{
			var rangeQuery = new DateRangeQuery(new Field("publishDate"))
			{
				Field = "publishDate",
				From = fromDateTime.ToString("yyyy-MM-dd"),
				To = toDateTime.ToString("yyyy-MM-dd")
			};

			var multiMatchQuery = new MultiMatchQuery
			{
				Fields = fields,
				Query = query,
				Fuzziness = new Fuzziness("AUTO")
			};

			var boolQuery = new BoolQuery
			{
				Must = new List<Query>
				{
					rangeQuery,
					multiMatchQuery
				}
			};

			var searchRequest = new SearchRequest<ElasticArticleEntity>
			{
				Query = boolQuery,
				Sort = new List<SortOptions>()
				{
					SortOptions.Field(new Field("publishDate"), new FieldSort() {Order = SortOrder.Desc}),
				},
				From = (pageIndex - 1) * pageSize,
				Size = pageSize
			};

			var response = await _client.SearchAsync<ElasticArticleEntity>(searchRequest);

			if (response.IsValidResponse)
			{
				return FluentResults.Result.Ok(response.Documents.ToList());
			}

			var error = response.ElasticsearchServerError != null
				? response.ElasticsearchServerError.Error.ToString()
				: "Unknown error";
			var apiCallDetails = response.ApiCallDetails?.DebugInformation;
			var message = $"Failed to update ElasticArticleEntity: {error}, {apiCallDetails}";
			_logger.LogError(message);
			return FluentResults.Result.Fail<List<ElasticArticleEntity>>(message).WithError(error);
		}
	}
}
