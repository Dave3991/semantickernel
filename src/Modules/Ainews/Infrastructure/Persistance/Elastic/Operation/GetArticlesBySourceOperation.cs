using Elastic.Clients.Elasticsearch;
using Elastic.Clients.Elasticsearch.QueryDsl;
using FluentResults;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;

namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;

public class GetArticlesBySourceOperation
{
	private readonly ElasticsearchClient _client;
	private readonly ILogger<GetArticlesBySourceOperation> _logger;

	public GetArticlesBySourceOperation(ElasticConnector connector, ILogger<GetArticlesBySourceOperation> logger)
	{
		_client = connector.GetClient();
		_logger = logger;
	}
	
	public async Task<Result<List<ElasticArticleEntity>>> GetArticlesBySourceAsync(List<string> sources)
	{
		var searchRequest = new SearchRequest<ElasticArticleEntity>
		{
			Query = new WildcardQuery(new Field("source"))
			{
				Value = sources.ToString(),
				CaseInsensitive = false
			}
		};

		var response = await _client.SearchAsync<ElasticArticleEntity>(searchRequest);
		
		if (response.IsValidResponse)
		{
			return FluentResults.Result.Ok(response.Documents.ToList());
		}
		else
		{
			var error = response.ElasticsearchServerError != null
				? response.ElasticsearchServerError.Error.ToString()
				: "Unknown error";
			var message = $"Failed to get Articles by source: {error}";
			_logger.LogError(message);
			return FluentResults.Result.Fail<List<ElasticArticleEntity>>(message);
		}
	}
}
