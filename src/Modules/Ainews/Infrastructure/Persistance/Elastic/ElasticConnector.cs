using System.Security.Cryptography.X509Certificates;
using Elastic.Clients.Elasticsearch;
using Elastic.Transport;

namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic
{
	public class ElasticConnector
	{
		private readonly ElasticsearchClient _client;

		public ElasticConnector(IConfiguration configuration)
		{
			var elasticSearchUrl = Environment.GetEnvironmentVariable("ELASTIC_SEARCH_URL") ??
			                       throw new Exception("No elastic search URL");
			var settings = new ElasticsearchClientSettings(new Uri(elasticSearchUrl))
				.DefaultIndex("ainews-articles")
				.EnableDebugMode()
				.ServerCertificateValidationCallback((sender, certificate, chain, sslPolicyErrors) => true)
				.RequestTimeout(TimeSpan.FromMinutes(2))
				.Authentication(new BasicAuthentication("elastic", "changeme"));
				// .Authentication(new ApiKey("QkpjSUs1QUJzNlh0WjNTcHdHbkE6MnQwYTZtVTVRUHFVd1pON1ZVanBIQQ=="));

			_client = new ElasticsearchClient(settings);
		}

		public ElasticsearchClient GetClient()
		{
			return _client;
		}
	}
}
