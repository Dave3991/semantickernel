namespace SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.DTOs;

public class ArticleSourcesDto
{
	public required List<string> Sources { get; set; }
}
