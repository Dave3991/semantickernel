namespace SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.DTOs;

public class NewsTrendsDto
{
	public required string Topic { get; set; }
	public required string TrendDescription { get; set; }
}
