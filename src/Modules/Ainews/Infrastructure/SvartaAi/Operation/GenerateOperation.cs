using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentResults;
using Microsoft.Extensions.Logging;
using Microsoft.SemanticKernel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Polly;
using Polly.Retry;
using SemanticKernel.Modules.Ainews.Infrastructure.Persistance.PostgreSQL.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.Operation.Responses;

namespace SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.Operation
{
    public class GenerateOperation
    {
        private readonly ILogger<GenerateOperation> _logger;
        private readonly GetArticleTopicsOperation _getArticleTopicsOperation;
        private readonly AsyncRetryPolicy<HttpResponseMessage> _retryPolicy;

        public GenerateOperation(ILogger<GenerateOperation> logger, GetArticleTopicsOperation getArticleTopicsOperation)
        {
            _logger = logger;
            _getArticleTopicsOperation = getArticleTopicsOperation;
            _retryPolicy = Policy
                .HandleResult<HttpResponseMessage>(r => !r.IsSuccessStatusCode)
                .WaitAndRetryAsync(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(30, retryAttempt)),
                    onRetry: (response, timespan, retryCount, context) =>
                    {
                        _logger.LogWarning($"Request failed with {response.Result.StatusCode}. Waiting {timespan} before next retry. Retry attempt {retryCount}");
                    });
        }

        public async Task<Result<LLMResponse>> Generate(string prompt, string model)
        {
            var client = new SvartaLLMConnector().CreateHttpClient();
            client.Timeout = TimeSpan.FromMinutes(5);

            var response = await _retryPolicy.ExecuteAsync(async () =>
            {
	            var request = new HttpRequestMessage(HttpMethod.Post, "/api/generate");
	            var payload = new
	            {
		            model,
		            stream = false,
		            format = "json",
		            prompt,
		            raw = false
	            };
	            _logger.LogInformation("Sending request to {url} with payload: {payload}", request.RequestUri, JsonConvert.SerializeObject(payload));
	            request.Content = new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json");
	            return await client.SendAsync(request);
            });

            var responseString = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
	            _logger.LogError("Failed to receive successful response after retries: {responseString}", responseString);
	            return Result.Fail("Failed to receive successful response after retries").WithError(response.ReasonPhrase);
            }

            _logger.LogInformation("Received response from Svarta LLM: {responseString}", responseString);
            var llmresponse = JsonConvert.DeserializeObject<LLMResponse>(responseString);
            if (llmresponse == null || llmresponse.Done == false)
            {
	            return Result.Fail("LLM response is not done yet");
            }
            return Result.Ok(llmresponse);
        }

        public async Task<Result<LLMResponse>> GenerateWithSemanticKernel(string prompt, string model)
        {
            var kernel = new SvartaLLMConnector().CreateKernelAiTextGeneration(model);
            var kernelArgs = new KernelArguments();
            kernelArgs.Add("prompt", prompt);
            kernelArgs.Add("stream", false);
            kernelArgs.Add("format", "json");
            kernelArgs.Add("apiKey", "json");
            kernelArgs.Add("raw", true);

            var fresult = await kernel.InvokePromptAsync(prompt, kernelArgs);
            var value = fresult.GetValue<string>();
            return Result.Ok(JsonConvert.DeserializeObject<LLMResponse>(value));
        }

        public async Task<Result<GetTopicFromTextResponseDTO>> GetTopicFromText(string text)
        {
            var availableTopics = _getArticleTopicsOperation.GetEnabledArticleTopics();
            if (!availableTopics.Any())
            {
                return Result.Fail<GetTopicFromTextResponseDTO>("No available topics.");
            }
            var responsePrompt = "Analyze the following text to identify relevant topics from the provided list and determine the text's value based on its informativeness, relevance to these topics, and potential impact. Respond in JSON format with two key-value pairs: one for the identified topics and another for the value assessment." +
                                 "\n\nKey 'topics' will contain a list of relevant topics from the provided list. Only select topics from the list provided. If no topics are found, the value will be an empty list in JSON format." +
                                 "\n\nKey 'value_assessment' will contain one of the following: 'high', 'medium', or 'low', indicating the value of the text.";
            var example = "Successful response example: {\n  \"topics\": [\"ai\", \"business\"],\n  \"value_assessment\": \"high\"\n} Unsuceessful response example: {\n  \"topics\": [],\n  \"value_assessment\": \"low\"\n}";
            var availableTopicsPropt = "The topics of interest are:\n" +
                                       string.Join("\n", availableTopics.Select(topic => $"- {topic.Name.Trim()}"));
            var prompt = $" {responsePrompt} {example} {availableTopicsPropt} \n\nText: ``` {text} ``` ";

            var response = await Generate(prompt, SvartaLLMConnector.model_llma3_3_70b_instruct_q4_k_m);
            if (response.IsFailed)
            {
                return Result.Fail<GetTopicFromTextResponseDTO>("Failed to generate response").WithErrors(response.Errors);
            }
            var responseJObject = JObject.Parse(response.Value.Response);
            var llmResponse = responseJObject.ToObject<GetTopicFromTextResponseDTO>();
            if (llmResponse == null || llmResponse.Topics == null || llmResponse.ValueAssessment == null)
            {
                return Result.Fail<GetTopicFromTextResponseDTO>($"Failed to deserialize response, {response.Value.Response} for prompt {prompt}");
            }
            return Result.Ok(llmResponse);
        }

        public async Task<Result<LLMResponse>> GetTextEvaluation(string text)
        {
            var topicsPrompt = "The topics of interest are: 'ai', 'politics', 'sports', 'health', 'business'.";
            var evaluatePrompt =
                "Evaluate the following text and determine its value based on informativeness, relevance to the provided topics, and potential impact." +
                " Respond in JSON format with a single key-value pair. The key is 'value_assessment' and the value is one of the following: 'high', 'medium', or 'low' indicating the value of the text. ";
            var prompt = $"{evaluatePrompt} {topicsPrompt} Text: {text}.";

            return await Generate(prompt, SvartaLLMConnector.model_llma3_3_70b_instruct_q4_k_m);
        }
    }
}
