using Newtonsoft.Json;

namespace SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.Operation;

public class GetTopicFromTextResponseDTO
{
	[JsonProperty("topics")]
	public List<string> Topics { get; set; }

	[JsonProperty("value_assessment")]
	public string ValueAssessment { get; set; }
}
