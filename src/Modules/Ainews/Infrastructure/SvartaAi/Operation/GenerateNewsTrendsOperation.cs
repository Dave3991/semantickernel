using System.Text;
using FluentResults;
using Newtonsoft.Json;
using SemanticKernel.Modules.Ainews.Domain.Entities.Elastic;
using SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.DTOs;

namespace SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.Operation;
/// <summary>
/// Generates trends from the given news articles
/// </summary>
public class GenerateNewsTrendsOperation
{
	
	private readonly GenerateOperation _generateOperation;

	public GenerateNewsTrendsOperation(GenerateOperation generateOperation)
	{
		_generateOperation = generateOperation;
	}

	public async Task<Result<NewsTrendsDto>> GenerateNewsTrendsAsync(List<ElasticArticleEntity> news, string topic)
	{
		var settings = new JsonSerializerSettings
		{
			NullValueHandling = NullValueHandling.Include,
			Formatting = Formatting.Indented, // For pretty printing, optional
			Converters = new List<JsonConverter> { new CustomDataConverter() }
		};
		
		// Select only the important fields from the news articles
		var newsImportant = news.Select(article => new
		{
			title = article.Title,
			content = article.Content,
			articleUrl = article.ArticleUrl,
			valueAssessment = article.CustomData["value_assessment"] ?? "N/A",
		}).ToList();
		
		// Convert the list of news articles to a JSON string
		var newsJson = JsonConvert.SerializeObject(newsImportant, settings);

		var successfulResponse = "Successful response example: \n``` [{\"title\":\"title1\",\"content\":\"content1\",\"aritcleUrl\":\"url1\"},{\"title\":\"title2\",\"content\":\"content2\",\"aritcleUrl\":\"url2\"}] ```";
		// Create a prompt for the LLM
		var prompt = $"Generate a comprehensive daily summary using the provided data from the last 24 hours. Ensure the summary is concise yet informative, highlighting the most important information from the past 24 hours. Use the provided article URLs as sources. Limit the number of articles to 5-7 to maintain focus on the most significant news. Structure the response in JSON format. {successfulResponse} Data:\n``` {newsJson} ```";
		
		// print the prompt to file
		//System.IO.File.WriteAllText("./prompt.txt", prompt);

		// Pass the prompt to the Generate method
		var response = await _generateOperation.Generate(prompt, SvartaLLMConnector.model_llma3_3_70b_instruct_q4_k_m);

		if (response.IsFailed)
		{
			return Result.Fail<NewsTrendsDto>("Failed to generate news trends").WithErrors(response.Errors);
		}

		var newsTrends = new NewsTrendsDto
		{
			Topic = topic,
			TrendDescription = response.Value.Response
		};

		return Result.Ok(newsTrends);
	}
}
