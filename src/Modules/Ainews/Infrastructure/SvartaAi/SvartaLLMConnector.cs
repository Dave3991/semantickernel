using System.Net.Http.Headers;
using Microsoft.Extensions.Azure;
using Microsoft.SemanticKernel;
using Microsoft.SqlServer.Server;

namespace SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi;

public class SvartaLLMConnector
{
	public const string model_phi3 = "phi3:14b-medium-128k-instruct-q5_K_M";
	public const string model_gemma2 = "gemma2:27b-instruct-q6_K";
	public const string model_llama3_70b_instruct = "llama3:70b-instruct";
	public const string model_llama3_70b_instruct_1m_context = "llama3-gradient:70b-instruct-1048k-q4_K_M";
	public const string model_mistral_large = "mistral-large:123b-instruct-2407-q2_K";
	public const string model_qwen2_5_32b_instruct = "qwen2.5:32b-instruct-q8_0";
	public const string model_llma3_3_70b_instruct_q4_k_m = "llama3.3:70b-instruct-q4_K_M";
	
	//creates http client
	public HttpClient CreateHttpClient()
	{
		var client = new HttpClient();
		client.BaseAddress = new Uri("http://llm.rig.bauerdavid.com");
		client.DefaultRequestHeaders.Accept.Clear();
		client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
		return client;
	}
	
	public Kernel CreateKernelAiTextGeneration(string modelId)
	{
		var kernelBuilder = Kernel.CreateBuilder();
		
		return kernelBuilder.Build();
	}
	
}
