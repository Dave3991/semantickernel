using Microsoft.SemanticKernel.ChatCompletion;
using OllamaSharp.Models.Chat;

namespace SemanticKernel.Modules.Ainews.Infrastructure.Ollama;

public class ChatHistoryService
{
	private readonly Dictionary<string, ChatHistory> userChatHistories = new Dictionary<string, ChatHistory>();

	private ChatHistory GetUserChatHistory(string userId)
	{
		if (!userChatHistories.ContainsKey(userId))
		{
			userChatHistories[userId] = new ChatHistory();
		}
		return userChatHistories[userId];
	}

	public void AddUserMessage(string message, string userId)
	{
		var chatHistory = GetUserChatHistory(userId);
		chatHistory.AddUserMessage(message);
	}

	public void AddSystemMessage(string message, string userId)
	{
		var chatHistory = GetUserChatHistory(userId);
		chatHistory.AddSystemMessage(message);
	}

	public void AddAssistantMessage(string message, string userId)
	{
		var chatHistory = GetUserChatHistory(userId);
		chatHistory.AddAssistantMessage(message);
	}

	public List<Message> GetChatHistory(string userId)
	{
		var chatHistory = GetUserChatHistory(userId);
		return chatHistory.Select(x => new OllamaSharp.Models.Chat.Message(x.Role.Label, x.Content)).ToList();
	}
}
