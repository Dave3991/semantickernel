using System.Text.Json;
using Microsoft.AspNetCore.SignalR;

namespace SemanticKernel.Modules.Ainews.UI.SignalR.Hubs;

public class ChatHub : Hub
{
	private readonly ILogger<ChatHub> _logger;

	public ChatHub(ILogger<ChatHub> logger)
	{
		_logger = logger;
	}

	/// <summary>
	/// this method is called by client(javascript) to get the connectionId, it should be negotiated and know, but its not
	/// </summary>
	public async Task GetConnectionId()
	{
		var connectionIdJson = new { connectionId = Context.ConnectionId };
		var jsonResponse = JsonSerializer.Serialize(connectionIdJson);
		await Clients.Caller.SendAsync("ReceiveConnectionId", jsonResponse);
	}
	
	/// <summary>
	/// Receive a message from the client and apply it to the chat
	/// </summary>
	/// <param name="message">The message to apply to the chat</param>
	public async Task ApplyMessage(string message)
	{
		_logger.LogInformation($"Received message: {message} from client: {Context.ConnectionId}");
	}
	

	
	
	
	public override async Task OnConnectedAsync()
	{
		_logger.LogInformation($"Client connected: {Context.ConnectionId}");
		var userId = Context.GetHttpContext().Request.Query["userId"];
		if (!string.IsNullOrEmpty(userId))
		{
			// Add the user to a group with their userId
			await Groups.AddToGroupAsync(Context.ConnectionId, userId);
		}
		await base.OnConnectedAsync();
	}

	// Optional: Override OnDisconnectedAsync to handle disconnections
	public override async Task OnDisconnectedAsync(Exception exception)
	{
		var userId = Context.GetHttpContext().Request.Query["userId"];
		if (!string.IsNullOrEmpty(userId))
		{
			// Remove the user from the group with their userId
			await Groups.RemoveFromGroupAsync(Context.ConnectionId, userId);
		}
		await base.OnDisconnectedAsync(exception);
	}
	
}
