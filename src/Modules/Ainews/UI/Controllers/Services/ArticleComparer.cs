using SemanticKernel.Modules.Ainews.Domain.Entities;
using SemanticKernel.Modules.Ainews.Domain.Entities.Postgres;

namespace SemanticKernel.Modules.Ainews.UI.Controllers.Services;

public class ArticleComparer : IEqualityComparer<object>
{
    public new bool Equals(object x, object y)
    {
        var lhs = x as ArticleEntity;
        var rhs = y as ArticleEntity;
        if (lhs == null || rhs == null) return false;
        return lhs.Id == rhs.Id; // or whatever makes them unique
    }

    public int GetHashCode(object obj)
    {
        return (obj as ArticleEntity)?.Id.GetHashCode() ?? 0;
    }
}