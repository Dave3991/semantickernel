namespace SemanticKernel.Modules.Ainews.UI.Controllers.Responses;

public class TopicsEntity
{
	public required int TopicId { get; set; }
	public required string TopicName { get; set; }
	public required int ArticleCount { get; set; }
}
public class ColumnEntity
{
	public required int columnIndex { get; set; }
	public required List<TopicsEntity> topics { get; set; }
	public List<string>? ResponseTopics { get; set; }
}

public class GetFeedsResponse
{
	public int FeedId { get; }
	public string FeedName { get;  }
	public List<ColumnEntity> ColumnEntities { get;}
	public GetFeedsResponse(int FeedId, string FeedName, List<ColumnEntity> columnEntities)
	{
		this.FeedId = FeedId;
		this.FeedName = FeedName;
		this.ColumnEntities = columnEntities;	
	}

	
}
