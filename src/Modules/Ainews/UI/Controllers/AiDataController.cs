using System.Runtime.CompilerServices;
using Azure.Search.Documents.Indexes.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SemanticKernel.Modules.Ainews.Domain.Case.TrendsCase;
using SemanticKernel.Modules.Ainews.Domain.Plugins;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi.Operation;
using SemanticKernel.Modules.Ainews.UI.Controllers.request;
using SemanticKernel.Modules.Ainews.UI.Controllers.Responses;
using Fields = Elastic.Clients.Elasticsearch.Fields;

namespace SemanticKernel.Modules.Ainews.UI.Controllers;

public class AiDataController: ControllerBase
{
    
    private readonly AzureDbContext _dbContext;
    private readonly FindTopicsInTextPlugin _findTopicsInTextPlugin;
    private readonly ILogger<AiDataController> _logger;
    private readonly GetArticlesOperation _getArticlesOperation;
    private readonly GetArticleSourcesOperation _getArticleSourcesAsync;
    private readonly GetArticlesBySourceOperation _getArticlesBySourceOperation;
    private readonly DeleteArticleOperation _deleteArticleOperation;
    private readonly GenerateOperation _generateOperation;
    private readonly TrendsCase _trendsCase;

    public AiDataController(AzureDbContext dbContext, FindTopicsInTextPlugin findTopicsInTextPlugin, ILogger<AiDataController> logger, GetArticlesOperation getArticlesOperation, GetArticleSourcesOperation getArticleSourcesAsync, GetArticlesBySourceOperation getArticlesBySourceOperation, DeleteArticleOperation deleteArticleOperation, GenerateOperation generateOperation, GenerateNewsTrendsOperation generateNewsTrendsOperation, TrendsCase trendsCase)
    {
        _dbContext = dbContext;
        _findTopicsInTextPlugin = findTopicsInTextPlugin;
        _logger = logger;
        _getArticlesOperation = getArticlesOperation;
        _getArticleSourcesAsync = getArticleSourcesAsync;
        _getArticlesBySourceOperation = getArticlesBySourceOperation;
        _deleteArticleOperation = deleteArticleOperation;
        _generateOperation = generateOperation;
        _trendsCase = trendsCase;
    }

    [HttpGet]
    [Route("api/ainews/topics")]
    public async Task<IActionResult> GetTopics()
    {
        // use linq to get the topics with the count of articles
         var topics = from topic in _dbContext.TopicEntity 
	         where topic.IsEnabled == true
            select new { TopicId = topic.Id, topicName = topic.Name, type = topic.Type};
        
        return Ok(topics);
    }
    

    [HttpPost]
    [Route("api/ainews/topics-from-text")]
    public async Task<IActionResult> GetTopicsFromText([FromBody]GetTopicsFromTextRequest request)
    {
        _logger.LogInformation("Finding topics in text: {text}", request.Text);
        var topics = await _findTopicsInTextPlugin.FindTopicsInText(request.Text);
        _logger.LogInformation("Found topics: {topics}", JsonConvert.SerializeObject(topics));
        return Ok(topics);
    }
    
    [HttpPost]
    [Route("api/ainews/articles-from-elastic")]
    public async Task<IActionResult> GetArticlesFromElastic([FromBody] GetArticlesFromElasticRequest request)
    {
	    var fieldDescriptions = request.Field.Select(field => field.GetDescription()).ToList();
	    Fields fields = fieldDescriptions.ToArray() ?? throw new InvalidOperationException("Fields cannot be null");
	    
	    _logger.LogInformation("Searching for articles with the query: {query} in Fields: {fields}", request.SearchValue, request.Field.ToString());
	    var query = string.Join(" ", request.SearchValue);
	    var result = await _getArticlesOperation.GetArticlessAsync(fields, query, request.PageIndex, request.PageSize, request.FromDateTime, request.ToDateTime);

	    if (result.IsSuccess)
	    {
		    return Ok(result.Value);
	    }
	    return BadRequest(result.Errors);
    }
    
    [HttpGet]
    [Route("api/ainews/get-topics-for-feed")]
    public async Task<IActionResult> GetTopicsForFeed(GetTopicsForFeedRequest request)
	{
	    var topics = from collection in _dbContext.CollectionEntity
		    join topicCollection in _dbContext.TopicCollectionEntity on collection.Id equals topicCollection.CollectionId
		    join topic in _dbContext.TopicEntity on topicCollection.TopicId equals topic.Id
		    where collection.Id == request.FeedId
		    select new { TopicId = topic.Id, topicName = topic.Name, type = topic.Type};
	    return Ok(topics);
	}
    
    [HttpGet]
    [Route("api/ainews/feeds")]
    public async Task<IActionResult> GetFeeds()
	{
		
		var feeds = await _dbContext.CollectionEntity.ToListAsync();
		var response = new List<GetFeedsResponse>();
		foreach (var feed in feeds)
		{
			var topics = from collection in _dbContext.CollectionEntity
				join topicCollection in _dbContext.TopicCollectionEntity on collection.Id equals topicCollection.CollectionId
				join topic in _dbContext.TopicEntity on topicCollection.TopicId equals topic.Id
				where collection.Id == feed.Id
				select new { TopicId = topic.Id, topicName = topic.Name, type = topic.Type, ColumnIndex = topicCollection.ColumnIndex};
			var columns = new List<ColumnEntity>();
			foreach (var topic in topics)
			{
				var existingColumn = columns.FirstOrDefault(c => c.columnIndex == topic.ColumnIndex);
				if (existingColumn != null)
				{
					// If column with same columnIndex exists, add new TopicsEntity into existing TopicsEntity list
					existingColumn.topics.Add(new TopicsEntity
					{
						TopicId = topic.TopicId, 
						TopicName = topic.topicName, 
						ArticleCount = 0
					});
				}
				else
				{
					// If column with same columnIndex doesn't exist, create new ColumnEntity
					columns.Add(
						new ColumnEntity
						{
							columnIndex = topic.ColumnIndex,
							topics = new List<TopicsEntity>
							{
								new TopicsEntity
								{
									TopicId = topic.TopicId, 
									TopicName = topic.topicName, 
									ArticleCount = 0
								}
							}
						}
					);
				}
			}
			
			response.Add(new GetFeedsResponse(feed.Id, feed.CollectionName, columns));
			
		}
		return Ok(response);
	}
    
    /// <summary>
    /// Get sources from the articles based on the wildcard query
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    [Route("api/ainews/get-sources")]
    public async Task<IActionResult> GetSources([FromBody] GetSourcesRequest request)
	{
	    var result = await _getArticleSourcesAsync.GetArticleSourcesAsync(request.Query);

	    if (result.IsSuccess)
	    {
		    return Ok(result.Value);
	    }
	    
		return BadRequest(result.Errors);
	}

	[HttpPost]
	[Route("api/ainews/get-articles-by-source")]
	public async Task<IActionResult> GetArticlesBySource([FromBody] GetArticlesBySourceRequest request)
	{
		var result = await _getArticlesBySourceOperation.GetArticlesBySourceAsync(request.Sources);

		if (result.IsSuccess)
		{
			return Ok(result.Value);
		}
		
		return BadRequest(result.Errors);
	}
	
	[HttpDelete]
	[Route("api/ainews/delete-article")]
	public async Task<IActionResult> DeleteArticle([FromQuery] string id)
	{
		var result = await _deleteArticleOperation.DeleteArticleAsync(id);

		if (result.IsSuccess)
		{
			return Ok();
		}
		
		return BadRequest(result.Errors);
	}
	
	[HttpPost]
	[Route("api/ainews/generate-topics")]
	public async Task<IActionResult> Generate([FromBody] GenerateTopicRequest request)
	{
		var result = await _generateOperation.GetTopicFromText(request.Text);

		return Ok(result);
	}
	
	[HttpPost]
	[Route("api/ainews/evaluate-text")]
	public async Task<IActionResult> EvaluateText([FromBody] GenerateTopicRequest request)
	{
		var result = await _generateOperation.GetTextEvaluation(request.Text);

		return Ok(result);
	}
	
	[HttpGet]
	[Route("api/ainews/trends")]
	public async Task<IActionResult> GetTrends()
	{
		var result = await _trendsCase.GetTrendsAsync();

		if (result.IsSuccess)
		{
			return Ok(result.Value);
		}
		
		return BadRequest(result.Errors);
	}

}
