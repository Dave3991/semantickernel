namespace SemanticKernel.Modules.Ainews.UI.Controllers.request;

public class GetTopicsFromTextRequest
{
    public required string Text { get; set; }
}