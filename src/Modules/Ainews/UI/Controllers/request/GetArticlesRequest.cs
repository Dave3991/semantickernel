namespace SemanticKernel.Modules.Ainews.UI.Controllers.request;

public class GetArticlesRequest
{
    public required List<string> Topics { get; set; }
}