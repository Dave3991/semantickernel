namespace SemanticKernel.Modules.Ainews.UI.Controllers.request;

public record GetSourcesRequest(string Query);
