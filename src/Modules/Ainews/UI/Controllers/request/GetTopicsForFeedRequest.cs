namespace SemanticKernel.Modules.Ainews.UI.Controllers.request;

public class GetTopicsForFeedRequest
{
	public required string FeedName { get; set; }
	public required int FeedId { get; set; }
}
