namespace SemanticKernel.Modules.Ainews.UI.Controllers.request;

public record GetArticlesBySourceRequest(List<string> Sources);
