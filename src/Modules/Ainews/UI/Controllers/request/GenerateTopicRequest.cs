namespace SemanticKernel.Modules.Ainews.UI.Controllers.request;

public class GenerateTopicRequest
{
	public required string Text { get; set; }
}
