namespace SemanticKernel.Modules.Ainews.UI.Controllers.request;

public class GetArticlesByTopicsRequest
{
    public required List<string> Topics { get; set; }
}