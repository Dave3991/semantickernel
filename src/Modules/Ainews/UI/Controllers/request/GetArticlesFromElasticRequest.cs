using System.ComponentModel;
using System.Reflection;
using System.ComponentModel;
using System.Runtime.InteropServices.JavaScript;
using UglyToad.PdfPig.Content;

namespace SemanticKernel.Modules.Ainews.UI.Controllers.request;

public class GetArticlesFromElasticRequest
{
	public required SearchField[] Field { get; set; }
	public required List<string> SearchValue { get; set; }
	
	public required int PageSize { get; set; }
	public required int PageIndex { get; set; }
	
	public required DateTime FromDateTime { get; set; }
	public required DateTime ToDateTime { get; set; }
}

public enum SearchField
{
	[Description("content")]
	Content = 0,

	[Description("topics")]
	Topics = 1,

	[Description("sources")]
	Sources = 2,
}
public static class EnumExtensions
{
	public static string GetDescription(this Enum value)
	{
		FieldInfo field = value.GetType().GetField(value.ToString());

		DescriptionAttribute attribute
			= Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute))
				as DescriptionAttribute;

		return attribute == null ? value.ToString() : attribute.Description;
	}
}
