using Microsoft.KernelMemory;
using PdfSharpCore.Pdf.Filters;

namespace SemanticKernel.Modules.Ainews.UI.Controllers.request;

public record GetNewsRequest(string Question, RequestMemoryFilter? Filter, string? Index);

public abstract record RequestMemoryFilter(string Tag, string Value);