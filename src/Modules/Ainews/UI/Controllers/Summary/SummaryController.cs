using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using OllamaSharp;
using OllamaSharp.Models.Chat;
using SemanticKernel.Modules.Ainews.Infrastructure.Ollama;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic.Operation;
using SemanticKernel.Modules.Ainews.Infrastructure.SvartaAi;
using SemanticKernel.Modules.Ainews.UI.SignalR.Hubs;

namespace SemanticKernel.Modules.Ainews.UI.Controllers.Summary;

[ApiController]
[Route("api/[controller]")]
public class SummaryController  : ControllerBase
{
	private readonly ILogger<SummaryController> _logger;
	
	private readonly IHubContext<ChatHub> _hubContext;

	private readonly ChatHistoryService _chatHistory;
	
	private readonly GetArticlesOperation _getArticlesOperation;

	public SummaryController(ILogger<SummaryController> logger, IHubContext<ChatHub> hubContext, ChatHistoryService chatHistory, GetArticlesOperation getArticlesOperation)
	{
		_logger = logger;
		_hubContext = hubContext;
		_chatHistory = chatHistory;
		_getArticlesOperation = getArticlesOperation;
	}
	
	[HttpPost("api/chat")]
	public async Task<IActionResult> Chat([FromBody] SemanticKernel.Modules.Ainews.UI.Controllers.Summary.Requests.ChatRequest request)
	{
		var ollamaChat = new OllamaChatCompletionService
		{
			ModelUrl = "http://llm.rig.bauerdavid.com",
			ModelName = SvartaLLMConnector.model_llma3_3_70b_instruct_q4_k_m
		};
		var data = await _getArticlesOperation.GetArticlessAsync(null, "technology", 1, 1000, DateTime.Now.AddDays(-1), DateTime.Now);

		
		foreach (var msg in request.Messages)
		{
			// replace placeholder {data} with actual data
			if (msg.Content.Contains("{data}"))
			{
				if(data.IsFailed)
				{
					_logger.LogError("Failed to get news {news}", data.Errors);
					return BadRequest("Failed to get news");
				}
				var newsImportant = data.Value.Select(article => new
				{
					title = article.Title,
					content = article.Content,
					articleUrl = article.ArticleUrl,
				}).ToList();
				
				msg.Content = msg.Content.Replace("{data}", JsonSerializer.Serialize(newsImportant));
			}
			_chatHistory.AddUserMessage(msg.Content, request.UserId); // Assuming AddUserMessage correctly handles the message role
		}

		var ollama = new OllamaApiClient(ollamaChat.ModelUrl, ollamaChat.ModelName);

		var historyMessages = _chatHistory.GetChatHistory(request.UserId);
		var streamChat = ollama.StreamChat(new ChatRequest()
		{
			Model = ollamaChat.ModelName,
			Stream = true,
			Messages = historyMessages
		}).AsAsyncEnumerable();

		var completeMessage = new StringBuilder();
		await foreach (var responseStream in streamChat)
		{
			var chatResponse = new ChatResponse
			{
				Model = responseStream.Model,
				CreatedAt = responseStream.CreatedAt,
				Message = new Message
				{
					Content = responseStream.Message.Content,
					Role = responseStream.Message.Role.ToString()
				},
				Done = false//responseStream.Done,
			};

			var jsonResponse = JsonSerializer.Serialize(chatResponse);
			await _hubContext.Clients.Client(request.ConnectionId).SendAsync("ReceiveMessage", jsonResponse);
		}
		
		var lastChatResponse = new ChatResponse
		{
			Model = ollamaChat.ModelName,
			CreatedAt = DateTime.Now.ToString(),
			Message = new Message
			{
				Content = "",
				Role = "assistant"
			},
			Done = true,
		};

		await _hubContext.Clients.Client(request.ConnectionId).SendAsync("ReceiveMessage", lastChatResponse);
		_chatHistory.AddAssistantMessage(completeMessage.ToString(), request.UserId);

		return Ok(JsonSerializer.Serialize("Chat completed"));
	}
	
}
