using Newtonsoft.Json;

namespace SemanticKernel.Modules.Ainews.UI.Controllers.Summary.Requests;

public class ChatRequest
{
	public string UserId { get; set; }
	public string ConnectionId { get; set; }
	
	public string Model { get; set; }
	
	public List<Message> Messages { get; set; }
	
	public Dictionary<string, object> Options { get; set; }
}

public class Message
{
	[JsonProperty("role")]
	public string Role { get; set; }
	[JsonProperty("content")]
	public string Content { get; set; }
	[JsonProperty("images")]
	public List<string>? Images { get; set; }
}
