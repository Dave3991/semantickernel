using Microsoft.AspNetCore.Mvc;
using SemanticKernel.Modules.Ainews.Domain.Case.ScrapeRedditCase;
using SemanticKernel.Modules.Ainews.Domain.Plugins;
using SemanticKernel.Modules.Ainews.UI.Controllers.request;

namespace SemanticKernel.Modules.Ainews.UI.Controllers;

public class AiNewsController: ControllerBase
{
    private readonly ScrapeRedditCase _scrapeRedditCase;
    private readonly FindTopicService _findTopicService;
    private readonly FindTopicsInTextPlugin _findTopicsInTextPlugin;

    public AiNewsController(ScrapeRedditCase scrapeRedditCase, FindTopicService findTopicService, FindTopicsInTextPlugin findTopicsInTextPlugin)
    {
        _scrapeRedditCase = scrapeRedditCase;
        _findTopicService = findTopicService;
        _findTopicsInTextPlugin = findTopicsInTextPlugin;
    }

   
    
    [HttpGet]
    [Route("api/ainews/scrape/reddit")]
    public async Task<IActionResult> ScrapeReddit()
    {
        await _scrapeRedditCase.Execute();
        return Ok();
    }
    
    [HttpPost]
    [Route("api/ainews/user")]
    public async Task<IActionResult> CreateUserGoals(string userContext)
    {
        return Ok();
    }
    
    
}
