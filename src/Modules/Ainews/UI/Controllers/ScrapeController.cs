using Microsoft.AspNetCore.Mvc;
using SemanticKernel.Modules.Ainews.Domain.Case.ScrapeTwitterCase;
using SemanticKernel.Modules.Ainews.Domain.DTOs;

namespace SemanticKernel.Modules.Ainews.UI.Controllers;

public class ScrapeController: ControllerBase
{
	private ScrapeTwitterCase _scrapeTwitterCase;

	public ScrapeController(ScrapeTwitterCase scrapeTwitterCase)
	{
		_scrapeTwitterCase = scrapeTwitterCase;
	}
	
	[HttpGet]
	[Route("api/scrape/twitter")]
	public async Task<IActionResult> ScrapeTwitter([FromQuery] ScrapeTwitterCaseDto request)
	{
		var result = await _scrapeTwitterCase.ExecuteAsync(request);
		if (result.IsFailed)
		{
			return BadRequest(result.Errors);
		}
		return Ok(result.Value);
	}
	
}
