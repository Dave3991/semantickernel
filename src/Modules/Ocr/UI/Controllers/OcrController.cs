using Microsoft.AspNetCore.Mvc;
using SemanticKernel.Modules.Ocr.Infrastructure;
using SemanticKernel.Modules.Ocr.UI.Controllers.Request;

namespace SemanticKernel.Modules.Ocr.UI.Controllers;

[ApiController]
[Produces("application/json")]
[Route("[controller]")]
public class OcrController: ControllerBase
{
    private readonly ILogger<OcrController> _logger;
    
    private readonly ParsePdf _parsePdf;

    public OcrController(ILogger<OcrController> logger, ParsePdf parsePdf)
    {
        _logger = logger;
        _parsePdf = parsePdf;
    }
    
    [HttpPost]
    [Route("api/ocr/parse")]
    public async Task<IActionResult> Parse([FromForm]OcrRequest request)
    {
        //log request
        _logger.LogDebug("Ocr request: {@Request}", request);

        var result = await _parsePdf.Parse(request.File);
        
        return Ok(result);
    }
}