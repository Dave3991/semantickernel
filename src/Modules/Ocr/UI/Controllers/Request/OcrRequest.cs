namespace SemanticKernel.Modules.Ocr.UI.Controllers.Request;

public record OcrRequest()
{
    public required IFormFile  File { get; set;}
}