using System.Text;
using PdfSharpCore.Pdf.IO;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using UglyToad.PdfPig;

namespace SemanticKernel.Modules.Ocr.Infrastructure;

using Tesseract;
public class ParsePdf
{
    private readonly ILogger<ParsePdf> _logger;

    public ParsePdf(ILogger<ParsePdf> logger)
    {
        _logger = logger;
    }

    public async Task<string> Parse(IFormFile file)
    {
        //use ocrmypdf https://github.com/ocrmypdf/OCRmyPDF to scan pdf unreadable pdf files (by tesseract) and save it as a new readable pdf file
        // than read this new file
        
        using (PdfDocument document = PdfDocument.Open(file.OpenReadStream()))
        {
            StringBuilder text = new StringBuilder();
            foreach (var page in document.GetPages())
            {
                text.Append(page.GetWords());
            }

            return text.ToString();
        }
    }
}