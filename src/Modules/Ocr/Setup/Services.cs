
using SemanticKernel.Modules.Ocr.Infrastructure;

namespace SemanticKernel.Modules.Ocr.Setup;

public static class Services
{
    public static void ConfigureServices(IServiceCollection services)
    {
        services.AddTransient<ParsePdf>();
    }
  
}
