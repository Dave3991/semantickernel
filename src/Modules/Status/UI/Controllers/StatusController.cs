using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace SemanticKernel.Modules.Status.UI.Controllers;

using Microsoft.AspNetCore.Mvc;

[Route("api/[controller]")]
[ApiController]
public class StatusController : ControllerBase
{

	private readonly HealthCheckService _healthCheckService;

	public StatusController(HealthCheckService healthCheckService)
	{
		_healthCheckService = healthCheckService;
	}

	// GET api/status/health
    [HttpGet("health")]
    public async Task<IActionResult> HealthCheck()
    {
	    var report = await _healthCheckService.CheckHealthAsync();

	    var result = new
	    {
		    status = report.Status.ToString(),
		    totalDuration = report.TotalDuration,
		    entries = report.Entries.Select(e => new 
		    {
			    key = e.Key,
			    value = new 
			    {
				    status = e.Value.Status.ToString(),
				    duration = e.Value.Duration,
				    exception = e.Value.Exception?.Message,
				    data = e.Value.Data,
				    description = e.Value.Description
			    }
		    })
	    };

	    return report.Status == HealthStatus.Healthy
		    ? Ok(result)
		    : StatusCode(503, result);
    }
    

    // GET api/status/buildinfo
    [HttpGet("buildinfo")]
    public IActionResult BuildInfo()
    {
        var buildInfo = new
        {
	        
            CommitSha = Environment.GetEnvironmentVariable("GIT_COMMIT") ?? "Unknown",
            ProjectVersion = Environment.GetEnvironmentVariable("PROJECT_VERSION") ?? "0.0.0",
            BuildId = Environment.GetEnvironmentVariable("BUILD_ID") ?? "0000",
            GIT_BRANCH = Environment.GetEnvironmentVariable("GIT_BRANCH") ?? "0000",
            Pipeline = Environment.GetEnvironmentVariable("PIPELINE_ID") ?? "None",
            GitTag = Environment.GetEnvironmentVariable("GIT_TAG") ?? "None",
            Timestamp = Environment.GetEnvironmentVariable("TIMESTAMP") ?? "Unknown",
            BUILD_DATE = Environment.GetEnvironmentVariable("BUILD_DATE") ?? "Unknown"
        };

        return Ok(buildInfo);
    }
}
