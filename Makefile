MIN_MAKE_VERSION := 3.82

ifneq ($(MIN_MAKE_VERSION),$(firstword $(sort $(MAKE_VERSION) $(MIN_MAKE_VERSION))))
$(error GNU Make $(MIN_MAKE_VERSION) or higher required)
endif

.DEFAULT_GOAL:=help
export PROJECT_ROOT := $(shell pwd)
PS_SCRIPT := ./ci/Migration/AddEfMigration.ps1

##@ Development
runllma: ## Run python llma server api
	@./scripts/llma_api_server.sh
downllma: ## shutdown server
	docker kill llama-api-server
build: ## build server
	docker-compose -f ./docker-compose-local-dockerized-development.yml build
install-playwright: ## install playwright
	@./scripts/install_playwright.sh
deploy-to-digital-ocean: ## push image to digital ocean
	export IMAGE_NAME=semantickernel:latest && pwsh -File ./ci/deploy/droplet_deploy.ps1

##@ Migrations
add-migration: ## Run EF migration script
	@echo "Running EF migration script..."
	@pwsh -ExecutionPolicy Bypass -File $(PS_SCRIPT)
update-database: ## Update database
	@echo "Updating database..."
	@dotnet ef database update  --context AzureDbContext --project SemanticKernel.csproj

##@ Docker
docker-build: ## Build docker image
	export IMAGE_NAME=semantickernel:latest && ./ci/build/build_image.sh
docker-run: ## Run docker image
	docker run -it --rm -p 80:80 semantickernel:latest

##@ Tests
healthcheck: ## Run healthcheck
	 pwsh -File ci/healthcheck/healthcheck.ps1
helm-lint: ## Run helm lint
	helm lint ./ci/deploy/charts/
helm-dry-run: ## Run helm dry run
	helm install --dry-run --debug --namespace=semantickernel --create-namespace semantickernel ./ci/deploy/charts/ --set image.repository=registry.gitlab.com/dave3991/semantickernel --set image.tag=master
get-services: ## Get services
	kubectl get services

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
