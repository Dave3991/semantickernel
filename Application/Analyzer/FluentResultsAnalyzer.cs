namespace SemanticKernel.Application.Analyzer;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using System.Collections.Immutable;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class FluentResultsAnalyzer : DiagnosticAnalyzer
{
	public const string DiagnosticId = "FluentResultsAnalyzer";

	private static readonly DiagnosticDescriptor Rule = new DiagnosticDescriptor(DiagnosticId, "Use FluentResults for methods that can fail", "Method '{0}' can fail but does not return a Result or Result<T> object", "Design", DiagnosticSeverity.Warning, isEnabledByDefault: true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(Rule); } }

	public override void Initialize(AnalysisContext context)
	{
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeMethodDeclaration, SyntaxKind.MethodDeclaration);
	}

	private static void AnalyzeMethodDeclaration(SyntaxNodeAnalysisContext context)
	{
		var methodDeclaration = (MethodDeclarationSyntax)context.Node;

		// Check if the method can fail based on your own criteria
		if (MethodCanFail(methodDeclaration))
		{
			var returnType = methodDeclaration.ReturnType.ToString();

			// Check if the method returns a Result or Result<T> object
			if (!returnType.StartsWith("Result"))
			{
				var diagnostic = Diagnostic.Create(Rule, methodDeclaration.GetLocation(), methodDeclaration.Identifier.ValueText);
				context.ReportDiagnostic(diagnostic);
			}
		}
	}
	private static bool MethodCanFail(MethodDeclarationSyntax methodDeclaration)
	{
		// Implement your own logic to determine if a method can fail
		return false;
	}
}
