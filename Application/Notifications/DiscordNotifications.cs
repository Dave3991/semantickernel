using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using HealthChecks.UI.Core.Notifications;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Newtonsoft.Json;

namespace SemanticKernel.Application.Notifications
{
	public class DiscordNotifications
	{
		private readonly HttpClient _httpClient;
		private readonly string _webhookUrl;

		public DiscordNotifications()
		{
			_httpClient = new HttpClient();
			_webhookUrl = "https://discord.com/api/webhooks/1255884025290031225/R3Fltdd_ng0axPNq4vRCB3HNZbd3k-K0CEg1FOFQOYYOIM4TffGQyS6L83bU6eRHsdky";
		}

		public async Task SendNotification(HealthReport healthReport)
		{
			var content = new StringContent(JsonConvert.SerializeObject(healthReport), Encoding.UTF8, "application/json");
			await _httpClient.PostAsync(_webhookUrl, content);
		}
	}
}
