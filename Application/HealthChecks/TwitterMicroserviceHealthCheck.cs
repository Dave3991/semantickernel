using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace SemanticKernel.Application.HealthChecks;

public class TwitterMicroserviceHealthCheck : IHealthCheck
{
	private readonly ILogger<TwitterMicroserviceHealthCheck> _logger;
	private readonly HttpClient _httpClient;
	private readonly string _url;

	public TwitterMicroserviceHealthCheck(ILogger<TwitterMicroserviceHealthCheck> logger, HttpClient httpClient, IConfiguration configuration)
	{
		_logger = logger;
		_httpClient = httpClient;
		_url = "http://tw.svarta.bauerdavid.com";
	}
	
	public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
	{
		try
		{
			var response = await _httpClient.GetAsync(_url, cancellationToken);
			response.EnsureSuccessStatusCode();
			return HealthCheckResult.Healthy();
		}
		catch (Exception ex)
		{
			_logger.LogError(ex, "Twitter microservice health check failed");
			return HealthCheckResult.Unhealthy(ex.Message);
		}
	}
}
