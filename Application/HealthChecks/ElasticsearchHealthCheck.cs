using Elastic.Clients.Elasticsearch;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance.Elastic;

namespace SemanticKernel.Application.HealthChecks
{
	public class ElasticsearchHealthCheck : IHealthCheck
	{
		private readonly ElasticsearchClient _elasticClient;

		public ElasticsearchHealthCheck(ElasticConnector elasticClient)
		{
			_elasticClient = elasticClient.GetClient();
		}

		public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
		{
			var pingResponse = await _elasticClient.PingAsync(cancellationToken: cancellationToken);

			if (pingResponse.IsValidResponse && pingResponse.IsSuccess())
			{
				return HealthCheckResult.Healthy("Elasticsearch is healthy.");
			}

			return HealthCheckResult.Unhealthy("Elasticsearch is unhealthy. Response: " + pingResponse.DebugInformation);
		}
	}
}
