using Microsoft.Extensions.Diagnostics.HealthChecks;
using Npgsql;

namespace SemanticKernel.Application.HealthChecks;

public class PostgreSqlHealthCheck : IHealthCheck
{
	private readonly string _connectionString;

	private readonly ILogger<PostgreSqlHealthCheck> _logger;
	public PostgreSqlHealthCheck(IConfiguration configuration, ILogger<PostgreSqlHealthCheck> logger)
	{
		_connectionString = configuration.GetConnectionString("DefaultConnection") ?? throw new ArgumentNullException("PostgreSql connection string is missing");
		_logger = logger;
	}

	public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
	{
		try
		{
			using var connection = new NpgsqlConnection(_connectionString);
			await connection.OpenAsync(cancellationToken);

			return HealthCheckResult.Healthy();
		}
		catch (Exception ex)
		{
			_logger.LogError(ex, "PostgreSql health check failed");
			return HealthCheckResult.Unhealthy(ex.Message);
		}
	}
}
