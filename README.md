# SemanticKernel

start llma api server for using LLM models.

make runllma

use reqeust like this for longer responses:

```
{
  "max_tokens": 1500,
  "messages": [
    {
      "role": "system",
      "content": "You are a helpful assistant."
    },
    {
      "role": "user",
      "content": "Write poem about moon."
    }
  ]
}
```

## Deployment to k8s
- install gitlab agent into k8s cluster (gitlab will generate complete helm chart for you)
- config agent in project (.gitlab/agents/digital-ocean/config.yaml)
- create all the helm charts & profit

## k8s setup

### Install Ingress
```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

helm install ingress-nginx ingress-nginx/ingress-nginx --namespace kube-system
```

### verify
```
kubectl get pods -n kube-system
```


### delete deployment
```
kubectl delete deployment semantic-kernel-semantickernel
```

### debug ingress
```
check if ingress is working
kubectl get ingress -n kube-system
kubectl describe ingress -n kube-system
```

### helm validation
```
helm lint ci/deploy/charts
helm template ci/deploy/charts
```


describe service
```
kubectl describe service semantickernel -n semantickernel
check this
LoadBalancer Ingress:     67.207.72.162
Port:                     http  80/TCP
TargetPort:               http/TCP

kubectl get service semantickernel -n semantickernel

NAME             TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)        AGE
semantickernel   LoadBalancer   10.245.164.49   67.207.72.162   80:30094/TCP   22m
```

Run shit insied k8s
```
kubectl run -it --rm debug --image=busybox --restart=Never -- sh
# Inside the temporary pod, run:
wget -qO- http://10.244.0.204:80/health
```

```
kubectl logs <pod-name> -n semantickernel
will show logs form app
```

```
kubectl logs -l app.kubernetes.io/name=ingress-nginx -n kube-system
```

restart ingress
```
kubectl rollout restart deployment ingress-nginx-controller -n ingress-nginx
```

delete ingress
```
kubectl delete ingress semantickernel-http -n semantickernel

delete main ingress
k get pods --all-namespaces
k delete pod <pod-name> -n kube-system
```
