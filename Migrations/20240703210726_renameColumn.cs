﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SemanticKernel.Migrations
{
    /// <inheritdoc />
    public partial class renameColumn : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "source",
                schema: "ainews",
                table: "articles",
                newName: "ArticleUrl");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ArticleUrl",
                schema: "ainews",
                table: "articles",
                newName: "source");
        }
    }
}
