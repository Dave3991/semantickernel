﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace SemanticKernel.Migrations
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "ainews");

            migrationBuilder.CreateTable(
                name: "articles",
                schema: "ainews",
                columns: table => new
                {
                    article_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    title = table.Column<string>(type: "text", nullable: false),
                    content = table.Column<string>(type: "text", nullable: false),
                    image_url = table.Column<string>(type: "text", nullable: false),
                    source = table.Column<string>(type: "text", nullable: false),
                    publish_date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_articles", x => x.article_id);
                });

            migrationBuilder.CreateTable(
                name: "topics",
                schema: "ainews",
                columns: table => new
                {
                    topic_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    is_enabled = table.Column<bool>(type: "boolean", nullable: false),
                    ParentId = table.Column<int>(type: "integer", nullable: true),
                    type = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_topics", x => x.topic_id);
                    table.ForeignKey(
                        name: "FK_topics_topics_ParentId",
                        column: x => x.ParentId,
                        principalSchema: "ainews",
                        principalTable: "topics",
                        principalColumn: "topic_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "users",
                schema: "ainews",
                columns: table => new
                {
                    user_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    email = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.user_id);
                });

            migrationBuilder.CreateTable(
                name: "article_topics",
                schema: "ainews",
                columns: table => new
                {
                    article_id = table.Column<int>(type: "integer", nullable: false),
                    topic_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_article_topics", x => new { x.article_id, x.topic_id });
                    table.ForeignKey(
                        name: "FK_article_topics_articles_article_id",
                        column: x => x.article_id,
                        principalSchema: "ainews",
                        principalTable: "articles",
                        principalColumn: "article_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_article_topics_topics_topic_id",
                        column: x => x.topic_id,
                        principalSchema: "ainews",
                        principalTable: "topics",
                        principalColumn: "topic_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "collections",
                schema: "ainews",
                columns: table => new
                {
                    collection_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    user_id = table.Column<int>(type: "integer", nullable: false),
                    collection_name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_collections", x => x.collection_id);
                    table.ForeignKey(
                        name: "FK_collections_users_user_id",
                        column: x => x.user_id,
                        principalSchema: "ainews",
                        principalTable: "users",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "topic_collections",
                schema: "ainews",
                columns: table => new
                {
                    collection_id = table.Column<int>(type: "integer", nullable: false),
                    topic_id = table.Column<int>(type: "integer", nullable: false),
                    column_index = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_topic_collections", x => new { x.collection_id, x.topic_id });
                    table.ForeignKey(
                        name: "FK_topic_collections_collections_collection_id",
                        column: x => x.collection_id,
                        principalSchema: "ainews",
                        principalTable: "collections",
                        principalColumn: "collection_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_topic_collections_topics_topic_id",
                        column: x => x.topic_id,
                        principalSchema: "ainews",
                        principalTable: "topics",
                        principalColumn: "topic_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "ainews",
                table: "topics",
                columns: new[] { "topic_id", "is_enabled", "name", "ParentId", "type" },
                values: new object[] { 1, true, "Root", null, 0 });

            migrationBuilder.CreateIndex(
                name: "IX_article_topics_topic_id",
                schema: "ainews",
                table: "article_topics",
                column: "topic_id");

            migrationBuilder.CreateIndex(
                name: "IX_articles_content",
                schema: "ainews",
                table: "articles",
                column: "content",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_collections_user_id",
                schema: "ainews",
                table: "collections",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_topic_collections_topic_id",
                schema: "ainews",
                table: "topic_collections",
                column: "topic_id");

            migrationBuilder.CreateIndex(
                name: "IX_topics_ParentId",
                schema: "ainews",
                table: "topics",
                column: "ParentId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "article_topics",
                schema: "ainews");

            migrationBuilder.DropTable(
                name: "topic_collections",
                schema: "ainews");

            migrationBuilder.DropTable(
                name: "articles",
                schema: "ainews");

            migrationBuilder.DropTable(
                name: "collections",
                schema: "ainews");

            migrationBuilder.DropTable(
                name: "topics",
                schema: "ainews");

            migrationBuilder.DropTable(
                name: "users",
                schema: "ainews");
        }
    }
}
