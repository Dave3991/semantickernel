﻿using Microsoft.EntityFrameworkCore.Migrations;
using System.Security.Cryptography;
using System.Text;

#nullable disable

namespace SemanticKernel.Migrations
{
	/// <inheritdoc />
	public partial class addcontnet_hash : Migration
	{
		/// <inheritdoc />
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropIndex(
				name: "IX_articles_content",
				schema: "ainews",
				table: "articles");

			migrationBuilder.AddColumn<string>(
				name: "content_hash",
				schema: "ainews",
				table: "articles",
				type: "text",
				nullable: false,
				defaultValue: "");

			// Populate content_hash for existing rows
			migrationBuilder.Sql(@"
                UPDATE ainews.articles
                SET content_hash = SUBSTRING(
                    ENCODE(DIGEST(content, 'sha256'), 'hex'), 1, 64)
            ");

			migrationBuilder.CreateIndex(
				name: "IX_articles_content_hash",
				schema: "ainews",
				table: "articles",
				column: "content_hash",
				unique: true);
		}

		/// <inheritdoc />
		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropIndex(
				name: "IX_articles_content_hash",
				schema: "ainews",
				table: "articles");

			migrationBuilder.DropColumn(
				name: "content_hash",
				schema: "ainews",
				table: "articles");

			migrationBuilder.CreateIndex(
				name: "IX_articles_content",
				schema: "ainews",
				table: "articles",
				column: "content",
				unique: true);
		}
	}
}
