﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SemanticKernel.Migrations
{
    /// <inheritdoc />
    public partial class addTopicDesc : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "description",
                schema: "ainews",
                table: "topics",
                type: "text",
                nullable: true);

            migrationBuilder.UpdateData(
                schema: "ainews",
                table: "topics",
                keyColumn: "topic_id",
                keyValue: 1,
                column: "description",
                value: null);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "description",
                schema: "ainews",
                table: "topics");
        }
    }
}
