﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SemanticKernel.Migrations
{
    /// <inheritdoc />
    public partial class removetopicdesc : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "description",
                schema: "ainews",
                table: "topics");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "description",
                schema: "ainews",
                table: "topics",
                type: "text",
                nullable: true);

            migrationBuilder.UpdateData(
                schema: "ainews",
                table: "topics",
                keyColumn: "topic_id",
                keyValue: 1,
                column: "description",
                value: null);
        }
    }
}
