using System.Threading.RateLimiting;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.RateLimiting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using SemanticKernel.Application.HealthChecks;
using SemanticKernel.Application.Notifications;
using SemanticKernel.Modules.Ainews.Application.Tracking;
using SemanticKernel.Modules.Ainews.Infrastructure.persistance;
using SemanticKernel.Modules.Ainews.UI.SignalR.Hubs;
using SemanticKernel.Modules.Ocr.Setup;
using Serilog;

var options = new WebApplicationOptions
{
	Args = args,
	ContentRootPath = Directory.GetCurrentDirectory(),
	WebRootPath = "wwwroot"
};

var builder = WebApplication.CreateBuilder(options);

// Configure Serilog
Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builder.Configuration)
    .Enrich.FromLogContext()
    .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {RequestId} {Message:lj}{NewLine}{Exception}")
    .WriteTo.File("Logs/logs.txt", rollingInterval: RollingInterval.Day)
    .CreateLogger();

builder.Host.UseSerilog();

builder.Services.AddRateLimiter(_ => _
    .AddFixedWindowLimiter(policyName: "fixed", options =>
    {
        options.PermitLimit = 4;
        options.Window = TimeSpan.FromSeconds(12);
        options.QueueProcessingOrder = QueueProcessingOrder.OldestFirst;
        options.QueueLimit = 2;
    }));

// Database
builder.Services.AddPooledDbContextFactory<AzureDbContext>(options =>
    options.UseNpgsql(
        builder.Configuration.GetConnectionString("DefaultConnection"),
        providerOptions => providerOptions.EnableRetryOnFailure()
    )
);

builder.Services.AddDbContext<AzureDbContext>(options =>
    options.UseNpgsql(
        builder.Configuration.GetConnectionString("DefaultConnection"),
        providerOptions => providerOptions.EnableRetryOnFailure()
    )
);

// HealthChecks
builder.Services.AddHealthChecks()
	.AddCheck<PostgreSqlHealthCheck>(
		"Postgre SQL",
		HealthStatus.Unhealthy,
		new[] { "database" }
	)
	.AddCheck<TwitterMicroserviceHealthCheck>(
		"Twitter microservice",
		HealthStatus.Unhealthy,
		new[] { "twitter microservice" }
	)
	.AddCheck<ElasticsearchHealthCheck>(
		"Elasticsearch",
		HealthStatus.Unhealthy,
		new[] { "elasticsearch" }
	);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSignalR();


// HealthChecks UI
builder.Services.AddHealthChecksUI(setupSettings: setup =>
	{
		if (builder.Environment.IsProduction())
		{
			setup.AddHealthCheckEndpoint("default", "http://SEMANTICKERNEL.semantickernel.svc.cluster.local/health");
		} else
		{
			var healthCheckPort = Environment.GetEnvironmentVariable("HEALTH_CHECK_PORT") ?? "5006";
			setup.AddHealthCheckEndpoint("default", $"http://localhost:{healthCheckPort}/health");
		}
		
		setup.SetEvaluationTimeInSeconds(5);
		setup.MaximumHistoryEntriesPerEndpoint(60);
		setup.SetApiMaxActiveRequests(1);
		setup.SetMinimumSecondsBetweenFailureNotifications(60);
		// setup.AddWebhookNotification("webhook1", uri: "https://discord.com/api/webhooks/1255884025290031225/R3Fltdd_ng0axPNq4vRCB3HNZbd3k-K0CEg1FOFQOYYOIM4TffGQyS6L83bU6eRHsdky",
		// 	payload: "{ \"message\": \"Webhook report for [[LIVENESS]]: [[FAILURE]] - Description: [[DESCRIPTIONS]]\"}",
		// 	restorePayload: "{ \"message\": \"[[LIVENESS]] is back to life\"}");
			
	})
	.AddInMemoryStorage();

Services.ConfigureServices(builder.Services);
SemanticKernel.Modules.Ainews.Setup.Services.ConfigureServices(builder.Services);

var app = builder.Build();

app.MapHealthChecks("/health", new HealthCheckOptions
{
    Predicate = _ => true, // the Predicate property is used to filter which health checks are included in the response, true means all health checks are included
    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
});
app.UseHealthChecksUI(config =>
{
	config.UIPath = "/hc-ui";
});
app.UseStaticFiles();

app.UseMiddleware<TrackingMiddleware>();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    // app.UseSwagger();
    // app.UseSwaggerUI();
}
app.UseSwagger();
app.UseSwaggerUI();

//disable CORS
app.UseCors(builder => builder
    .WithOrigins("http://127.0.0.1:3000", "http://localhost:3000", "https://localhost:3000", "http://ainews.bauerdavid.com", "https://ainews.bauerdavid.com")
    .AllowAnyMethod()
    .AllowAnyHeader());

app.UseHttpsRedirection();

app.UseRateLimiter();

app.UseAuthorization();

app.MapControllers();
app.MapHub<ChatHub>("/chatHub");
app.UseDeveloperExceptionPage();

app.Run();
