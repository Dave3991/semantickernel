#!/usr/bin/env pwsh
### USAGE:
### .\AddEfMigration.ps1 -MigrationName "YourMigrationName"

param(
    [Parameter(Mandatory=$true)]
    [string]$MigrationName
)

$ErrorActionPreference = "Stop"

# Get the project root directory from ENV variable or ask user to enter it
$PROJECT_ROOT = $env:PROJECT_ROOT
if ($PROJECT_ROOT -eq $null) {
    $PROJECT_ROOT = Read-Host "Enter the project root directory"
}

# Change directory to the project directory if needed
# Set-Location "path\to\your\project\directory"

# Call dotnet ef to add migration
try {
    dotnet ef migrations add $MigrationName --project $PROJECT_ROOT/SemanticKernel.csproj --context AzureDbContext --startup-project $PROJECT_ROOT/SemanticKernel.csproj --output-dir $PROJECT_ROOT/Migrations
    Write-Host "Migration '$MigrationName' added successfully."
} catch {
    Write-Error "Failed to add migration: $_"
}
