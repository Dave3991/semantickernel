param(
  [Parameter(Mandatory=$false)]
  [string]$imageName
)

$ErrorActionPreference = "Stop"

# Check if the IMAGE_NAME environment variable is set
if ([string]::IsNullOrEmpty($imageName)) {
  $imageName = $env:IMAGE_NAME
}

# If the imageName is still null or empty, throw an error
if ([string]::IsNullOrEmpty($imageName)) {
  throw "Error: Image name is not provided and IMAGE_NAME environment variable is not set."
}

# do docker login to digital ocean
docker login registry.digitalocean.com -u dave3991@gmail.com -p "dop_v1_6422a237c377c4a41e9cb01d74e2fec6207ac157d2d3cce1c22769eb9a79f167"

# push the image to digital ocean
docker push $imageName