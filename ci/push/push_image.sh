#!/usr/bin/env bash
set -euo pipefail

echo "Pushing $IMAGE_NAME"

docker push "$IMAGE_NAME"

echo "Pushed $IMAGE_NAME"
