#!/usr/bin/env bash
set -euo pipefail

#set project root if not set
if [ -z "${PROJECT_ROOT:-}" ]; then
    # shellcheck disable=SC2155
    export PROJECT_ROOT="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../../")"
fi

echo "Building $IMAGE_NAME from PROJECT_ROOT: $PROJECT_ROOT"

docker build --pull \
  --cache-from "$IMAGE_NAME" \
  --tag "$IMAGE_NAME" \
  --build-arg IMAGE_NAME="${IMAGE_NAME}" \
  --build-arg PIPELINE_ID="${CI_PIPELINE_ID:-}" \
  --build-arg GIT_TAG="${CI_COMMIT_TAG:-}" \
  --build-arg TIMESTAMP="$(date +%s)" \
  --build-arg GIT_COMMIT="${CI_COMMIT_SHA:-}" \
  --build-arg GIT_BRANCH="${CI_COMMIT_REF_NAME:-}" \
  --build-arg GIT_REPO_NAME="${CI_PROJECT_NAME:-}" \
  --build-arg BUILD_DATE="$(date -u +\"%Y-%m-%dT%H:%M:%SZ\")" \
  "$PROJECT_ROOT"

# shellcheck disable=SC2005
echo "$(docker images --format "Image full name: {{.Repository}}:{{.Tag}} {{.Size}}" | grep "$IMAGE_NAME")"
