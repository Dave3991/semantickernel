#!/usr/bin/env bash
set -euo pipefail

echo "Logging into Azure"
az login --service-principal -u="$AZURE_APP_ID" -p="$AZURE_SECRET" --tenant="$AZURE_TENANT_ID"
echo "Successfully logged into Azure"