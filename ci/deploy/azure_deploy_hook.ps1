param(
    [Parameter(Mandatory=$false)]
    [string]$imageName
)

$ErrorActionPreference = "Stop"

# Check if the IMAGE_NAME environment variable is set
if ([string]::IsNullOrEmpty($imageName)) {
    $imageName = $env:IMAGE_NAME
}

# If the imageName is still null or empty, throw an error
if ([string]::IsNullOrEmpty($imageName)) {
    throw "Error: Image name is not provided and IMAGE_NAME environment variable is not set."
}

# Define the URL for the Azure webhook
$webhookUrl = "https://$ainews-backend:oux5n0HXt1t481Ddi3Kgsr76ka1zPN7rouvsvgDNj7by6NcJD8ECvPBEfKuP@ainews-backend.scm.azurewebsites.net/api/registry/webhook"


# Login to Azure
az login --service-principal -u="$env:AZURE_APP_ID" -p="$env:AZURE_SECRET" --tenant="$env:AZURE_TENANT_ID" | ConvertFrom-Json

# Get the access token
$tokenOutput = az account get-access-token | ConvertFrom-Json

# Extract the access token
$accessToken = $tokenOutput.accessToken

# Define the headers for the POST request
$headers = @{
    "Content-Type" = "application/json"
    "Authorization" = "Bearer $accessToken"
}

# Define the body for the POST request
$body = @{
    "image" = $imageName
} | ConvertTo-Json

# Send the POST request
Invoke-RestMethod -Uri $webhookUrl -Method Post -Body $body -Headers $headers