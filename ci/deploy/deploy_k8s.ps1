# Define variables
$imageRepository = $env:CI_REGISTRY_IMAGE
$imageTag = $env:CI_COMMIT_TAG
$chartPath = $env:PROJECT_ROOT+"ci/deploy/charts" # Path to your Helm chart
$releaseName = "semantic_kernel_release" # Name of the Helm release

# if CI_COMMIT_TAG is empty, use CI_COMMIT_REF_SLUG
if ([string]::IsNullOrEmpty($imageTag)) {
  $imageTag = $env:CI_COMMIT_REF_SLUG
}

if ([string]::IsNullOrEmpty($env:PROJECT_ROOT)) {
   Read-Host "Enter the project root directory"
}

# Check if Helm release exists
$releaseExists = helm list --filter "^$releaseName$" --short

if ($releaseExists) {
  # If release exists, upgrade
  helm upgrade $releaseName $chartPath --set image.repository=$imageRepository --set image.tag=$imageTag
} else {
  # If release does not exist, install
  helm install $releaseName $chartPath --set image.repository=$imageRepository --set image.tag=$imageTag
}