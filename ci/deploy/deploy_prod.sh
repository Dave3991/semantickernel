#!/usr/bin/env bash
set -euo pipefail

# Function to deploy the container
deploy_container() {
    if [[ -z "${IMAGE_NAME:-}" ]]; then
        echo "Error: IMAGE_NAME is not set."
        exit 1
    fi

    echo "Deploying container with image: $IMAGE_NAME"
    
    # Capture and display the output of the az container create command
    # resource group is the name of place where the container will be deployed
    DEPLOY_OUTPUT=$(az container create \
            --resource-group "$RESOURCE_GROUP"\
            --name semantickernel-container \
            --image "$IMAGE_NAME" \
            --cpu 1 \
            --memory 0.5 \
            --registry-login-server "$CI_REGISTRY" \
            --registry-username "dave3991@gmail.com" \
            --registry-password "$AZURE_GITLAB_REGISTRY_READ_TOKEN" \
            --dns-name-label semantickernel-container \
            --ports 80 443 \
            --environment-variables \
                OPENAI_API_KEY="$OPENAI_API_KEY" \
                CI_IMAGE_NAME="$IMAGE_NAME")
    
    # Check if the command was successful
    if [[ $? -ne 0 ]]; then
        echo "Error: Failed to deploy container - $IMAGE_NAME"
        echo "$DEPLOY_OUTPUT"
        exit 1
    fi

    echo "$DEPLOY_OUTPUT"
    echo "Successfully deployed container - $IMAGE_NAME"
    
    # Get the container AZURE_URL and pass it to deploy.env
    AZURE_URL=$(echo "$DEPLOY_OUTPUT" | jq -r '.ipAddress.fqdn')
    echo "AZURE_URL=$AZURE_URL" >> "$PROJECT_ROOT/deploy.env"
}

# Function to initialize environment variables
initialize_env() {
    # If PROJECT_ROOT is not set, initialize it
    if [[ -z "${PROJECT_ROOT:-}" ]]; then
        CURRENT_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
        export PROJECT_ROOT="$CURRENT_PATH/../../"
    fi
    
    # check if jq is installed
    if ! command -v jq &> /dev/null; then
        echo "Error: jq is not installed."
        exit 1
    fi
}

# Main script execution
main() {
    initialize_env
    echo "Deploying container with image: $IMAGE_NAME"
    deploy_container
}

# Execute the main function
main
