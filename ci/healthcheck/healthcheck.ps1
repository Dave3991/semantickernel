param(
    [Parameter(Mandatory=$true)]
    [int]$HealthCheckPort
)

Set-StrictMode -Version 2.0
$ErrorActionPreference = "Stop"

$healthCheckUrl = "http://localhost:$HealthCheckPort/api/status/health"

try {
    $response = Invoke-WebRequest -Uri $healthCheckUrl -UseBasicParsing
    if ($response.StatusCode -eq 200) {
        exit 0
    } else {
        # write out response body
        $response.Content
        exit 1
    }
} catch {
    $response.Content
    exit 1
}
