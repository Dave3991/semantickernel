#!/usr/bin/env bash
set -euo pipefail

# if $CI_COMMIT_TAG is not set or is empty, then we are not on a tag
if [ -z "${CI_COMMIT_TAG:-}" ]; then
    echo "IMAGE_NAME=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" >>build.env
    echo "K8S_IMAGE_TAG=$CI_COMMIT_REF_SLUG" >>build.env
else
    echo "IMAGE_NAME=$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" >>build.env
    echo "K8S_IMAGE_TAG=$CI_COMMIT_TAG" >>build.env
fi

# shellcheck disable=SC2002
echo "Set IMAGE_NAME to $(cat build.env | grep IMAGE_NAME | cut -d= -f2)"
# shellcheck disable=SC2002
echo "Set K8S_IMAGE_TAG to $(cat build.env | grep K8S_IMAGE_TAG | cut -d= -f2)"
